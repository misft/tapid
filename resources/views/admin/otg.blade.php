@extends('crudbooster::admin_template')
@section('content')
    <div class="col-xs-12" style="height: 450px; overflow-y: auto">
        <div class="text-justify">
            <a href="/admin/dashboard">
                <i class="fa fa-arrow-left"></i> Back to Dashboard
            </a>
            <h3>Status OTG</h3>
            <a href="download/otg" target="_blank">Print</a>
        </div>
        <table class="table table-responsive table-striped table-danger">
            <thead class="bg-primary">
            <tr>
                <th>Nama</th>
                <th>Mulai Tanggal</th>
                <th>Sampai Tanggal</th>
                <th>Sisa Hari</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($table['otg']))
                @foreach($table['otg'] as $i)
                    <tr>
                        <td>{{$i->name}}</td>
                        <td>{{CRUDBooster::parseDateIndo($i->start_at, false, true)}}</td>
                        <td>{{CRUDBooster::parseDateIndo($i->end_at, false, true)}}</td>
                        <td>{{$i->days_left}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection
