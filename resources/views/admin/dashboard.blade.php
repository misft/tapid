@extends('crudbooster::admin_template')
@push('head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
@endpush
@section('content')
    <div class="container-fluid row">
        <div class="col-xs-4">
            <div class="border-box">
                <div class="small-box bg-blue">
                    <div class="inner inner-box">
                        <h3>{{ $box['cluster_count']->total != 0 ?: 0  }} <p>
                                {{ $box['cluster_count']->plus != 0 ?: 0 }}</p></h3>
                        <h4>Jumlah Cluster</h4>
                    </div>
                    <div class="icon">
                        <i class="ion ion-people"></i>
                    </div>
                    <a href="admin/clusters" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="border-box">
                <div class="small-box bg-red">
                    <div class="inner inner-box">
                        <h3>{{ $box['otg_count']->total }} <p>{{ $box['otg_count']->plus }}</p>
                        </h3>
                        <h4>Jumlah OTG</h4>
                    </div>
                    <div class="icon">
                        <i class="ion ion-people"></i>
                    </div>
                    <a href="admin/otg" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="border-box">
                <div class="small-box bg-green">
                    <div class="inner inner-box">
                        <h3>{{ $box['quarantine_count']->total }} <p>
                                {{ $box['quarantine_count']->plus }}</p></h3>
                        <h4>Selesai Karantina</h4>
                    </div>
                    <div class="icon">
                        <i class="ion ion-people"></i>
                    </div>
                    <a href="admin/quarantine" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <canvas id="chart_cluster" style="width: 100%; height: 300px"></canvas>
        </div>
        <div class="col-xs-12">
            <h3>Keadaan Merchant</h3>
            <a href="admin/download/merchant_condition">Print</a>
            <canvas id="merchant_overload_pie" style="width: 100%; height: 300px"></canvas>
        </div>
        <div class="col-xs-12 col-lg-6" style="height: 450px; overflow-y: auto">
            <div class="text-justify">
                <h3>Merchant Over Kapasitas </h3>
                <a href="admin/download/overload_merchants">Print</a>
            </div>
            <table class="table table-responsive table-striped table-danger">
                <thead class="bg-fuchsia">
                <tr>
                    <th>Nama Merchant</th>
                    <th>Jumlah Pengunjung</th>
                    <th>Kapasitas Pendemi</th>
                </tr>
                </thead>
                <tbody>
                @foreach($table['overload_merchant'] as $i)
                    <tr>
                        <td>{{$i->name}}</td>
                        <td>{{$i->visitor_count}}</td>
                        <td>{{intval($i->capacity * $i->capacity_percentage)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-lg-6" style="height: 450px; overflow-y: auto">
            <div class="text-justify">
                <h3>Riwayat Merchant Over Kapasitas</h3>
                <a href="admin/download/overcapacity_history">Print</a>
            </div>
            <table class="table table-responsive table-striped table-danger">
                <thead class="bg-fuchsia">
                <tr>
                    <th>Nama</th>
                    <th>Point</th>
                    <th>Rata rata</th>
                </tr>
                </thead>
                <tbody>
                @foreach($table['overcapacity_history'] as $i)
                    <tr>
                        <td>{{$i->name}}</td>
                        <td>{{$i->sum}}</td>
                        <td>{{$i->average}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script>
        var responseVisitorRecently = {!! $chart['visitor_recently'] !!};
        var response
    </script>
    <script src="{{asset('js/admin/dashboard/chart_visitor_recently.js')}}"></script>

    <script>
        var responseClusters = {!! $chart['clusters'] !!}
        var responseOtg = {!! $chart['otg'] !!}
        var responseQuarantine = {!! $chart['quarantine'] !!}

        var clustersData = {
            dataset: [
                {
                    label: "Jumlah Cluster",
                    data: [],
                    borderWidth: 1,
                    borderColor: "#0073b7",
                    fill: "#0073b7",
                    backgroundColor: "#0073b7"
                },
                {
                    label: "Jumlah OTG",
                    data: [],
                    borderWidth: 1,
                    borderColor: "#dd4b39",
                    fill: "#dd4b39",
                    backgroundColor: "#dd4b39"
                },
                {
                    label: "Jumlah Quarantine",
                    data: [],
                    borderWidth: 1,
                    borderColor: "#00a65a",
                    fill: "#00a65a",
                    backgroundColor: "#00a65a"
                }
            ],
            labels: [],
        }
        responseClusters.forEach(e => {
            clustersData.labels.push(e.created_at)
            clustersData.dataset[0].data.push(e.sum)
        });
        responseOtg.forEach(e => {
            clustersData.labels.push(e.start_at)
            clustersData.dataset[1].data.push(e.sum)
        })
        responseQuarantine.forEach(e => {
            clustersData.labels.push(e.start_at)
            clustersData.dataset[2].data.push(e.sum)
        })
        var options = {
            type: 'line',
            data: {
                labels: clustersData.labels,
                datasets: clustersData.dataset
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            reverse: false
                        }
                    }]
                }
            }
        }

        var ctx = document.getElementById('chart_cluster').getContext('2d');
        new Chart(ctx, options);
    </script>
    <script>
        var responseMerchantOverload = {!! $pie['merchant_capacity'] !!}
        var dangerCount = 0
        var warningCount = 0
        var safeCount = 0
        responseMerchantOverload.forEach(e => {
            if (e.zone == 'Safe') safeCount++;
            if (e.zone == 'Warning') warningCount++;
            if (e.zone == 'Danger') dangerCount++;
        })
        var data = {
            datasets: [{
                data: [dangerCount, warningCount, safeCount],
                backgroundColor: [
                    "#f13939",
                    "#ecd03f",
                    "#7ef642",
                ]
            }],
            labels: [
                'Danger',
                'Warning',
                'Safe'
            ]
        }
        new Chart(document.getElementById('merchant_overload_pie').getContext('2d'), {
            type: 'pie',
            data: data
        });
    </script>
@endpush
