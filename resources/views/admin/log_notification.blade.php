@extends('crudbooster::admin_template')
@section('content')
    <!-- Your html goes here -->
    <div class='panel panel-default'>
        <div class='panel-heading'>

        </div>
        <div class="panel-body">
            <table class="table table-responsive table-bordered">
                <thead>
                <tr>
                    <th>Timestamp</th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>Pesan</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($row as $i)
                        <tr>
                            <td>{{$i->created_at}}</td>
                            <td>{{$i->name}}</td>
                            <td>{{$i->nik}}</td>
                            <td>{{$i->message}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
