@extends('crudbooster::admin_template')
@section('content')
    <div class="col-xs-12" style="height: 450px; overflow-y: auto">
        <div class="text-justify">
            <a href="/admin/dashboard">
                <i class="fa fa-arrow-left"></i> Back to Dashboard
            </a>
            <h3>Klasters </h3>
            <a href="download/clusters" target="_blank">Print</a>
        </div>
        <table class="table table-responsive table-striped table-danger">
            <thead class="bg-primary">
            <tr>
                <th>Nama Merchant</th>
                <th>Mulai dari</th>
                <th>Sampai</th>
                <th>Jumlah OTG</th>
                <th>Jumlah Karantina</th>
                <th>Sisa Karantina</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($table['clusters']))
                @foreach($table['clusters'] as $i)
                    <tr>
                        <td>{{$i->merchant_name}}</td>
                        <td>{{CRUDBooster::parseDateIndo($i->start_at, false, true)}}</td>
                        <td>{{CRUDBooster::parseDateIndo($i->end_at, false, true)}}</td>
                        <td>{{$i->otg_count}}</td>
                        <td>{{$i->quarantine_count}}</td>
                        <td>{{$i->quarantine_days_left}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection
