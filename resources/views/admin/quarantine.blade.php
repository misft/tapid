@extends('crudbooster::admin_template')
@section('content')
    <div class="col-xs-12" style="height: 450px; overflow-y: auto">
        <div class="text-justify">
            <a href="/admin/dashboard">
                <i class="fa fa-arrow-left"></i> Back to Dashboard
            </a>
            <h3>Status Selesai Karantina</h3>
            <a href="download/quarantine" target="_blank">Print</a>
        </div>
        <table class="table table-responsive table-striped table-danger">
            <thead class="bg-primary">
            <tr>
                <th>Nama</th>
                <th>Selesai Tanggal</th>
            </tr>
            </thead>
            <tbody>
            @if(!empty($table['quarantine']))
                @foreach($table['quarantine'] as $i)
                    <tr>
                        <td>{{$i->name}}</td>
                        <td>{{CRUDBooster::parseDateIndo($i->end_at, false, true)}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection
