@extends('crudbooster::admin_template')
@push('head')
    <script type="text/javascript" src="{{asset('lib/instascan/instascan.min.js')}}"></script>
    <style>
        #preview {
            width: 100%;
            height: 100%;
            max-height: 100vh;
        }
    </style>
@endpush
@section('content')
    <video id="preview"></video>
@endsection
@push('bottom')
    <script type="text/javascript">
        var scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 5, mirror: false });
        scanner.addListener('scan',function(content){
            $.ajax('/api/check_in_merchant', {
                method: 'POST',
                data: {
                    secret: content,
                    id_merchant: {{ CRUDBooster::myId() }}
                }
            }).then(res => {

            }).catch(res => {
                alert(res)
            })
        });
        Instascan.Camera.getCameras().then(function (cameras){
            if(cameras.length>0){
                scanner.start(cameras[0]);
            }else{
                console.error('No cameras found.');
                alert('No cameras found.');
            }
        }).catch(function(e){
            console.error(e);
            alert(e);
        });
    </script>
@endpush
