@extends('crudbooster::admin_template')
@push('head')
    <link rel="stylesheet" href="{{asset('lib/select2/select2.min.css')}}">
@endpush
@section('content')
    <!-- Your html goes here -->
    <div class='panel panel-default'>
        <div class='panel-heading'>Add Form</div>
        <div class='panel-body'>
            <form id="form" method='post' action='{{CRUDBooster::mainpath('edit-save/'.$row->id)}}'>
                {{csrf_field()}}
                <div class='form-group'>
                    <label>Nama Merchant</label>
                    <input type='text' name='name' required minlength="0" maxlength="30" class='form-control'
                           value="{{$row->name}}"/>
                </div>
                <div class='form-group'>
                    <label>Email</label>
                    <input type='text' name='email' required minlength="0" maxlength="30" class='form-control'
                           value="{{$row->email}}"/>
                </div>
                <div class='form-group'>
                    <label>Password</label>
                    <input type='password' name='password' minlength="8" maxlength="16" class='form-control'/>
                </div>
                <div class='form-group'>
                    <label>Konfirmasi Password</label>
                    <input type='password' name='confirmation_password' minlength="8" maxlength="16"
                           class='form-control'/>
                    <p id="password-warning" class="text-danger" style="display: none">Password merchant belum sama</p>
                    <p id="password-success" class="text-success" style="display: none">Password merchant sudah sama</p>
                </div>
                <div class='form-group'>
                    <label>Lokasi</label>
                    <div class="container p-4" style="background-color: aliceblue; width: 50%"
                         id="map-search-container">
                        <input type="text" class="form-control" id="map-search" placeholder="Cari Lokasi">
                    </div>
                    <div id="map" class="row" style="height: 200px"></div>
                    <input type='hidden' name='latitude' required class='form-control' value="{{$row->latitude}}"/>
                    <input type='hidden' name='longitude' required class='form-control' value="{{$row->longitude}}"/>
                </div>
                <div class='form-group'>
                    <label>Alamat</label>
                    <input type='text' name='address' required minlength="10" maxlength="100" class='form-control'
                           value="{{$row->address}}"/>
                </div>
                <div class='form-group'>
                    <label>Kapasitas</label>
                    <input type='text' name='capacity' required minlength="0" maxlength="99999999" class='form-control' value="{{$row->capacity}}"/>
                </div>
                <div class='form-group'>
                    <label>Jenis Bisnis</label>
                    <select class="form-control" name="bussiness_type_id" id=""></select>
                </div>
                <div class='form-group'>
                    <label>Jumlah lantai</label>
                    <input type="number" name='floor_count' class='form-control' value="{{$row->floor_count}}"/>
                </div>
                <div class='form-group'>
                    <label>Jenis Merchant</label>
                    <select name="merchant_type" class="form-control">
                        <option value="0">-- --</option>
                        <option value="1">Mall</option>
                        <option value="2">Perkantoran</option>
                        <option value="3">Pasar Tradisional (Wirausaha)</option>
                        <option value="4">Transportation Hub</option>
                        <option value="5">Tenant</option>
                    </select>
                </div>
                <div class='form-group'>
                    <label>Dari Jenis Merchant</label>
                    <select name="merchant_type_of" class="form-control">
                        <option value="0">-- --</option>
                        <option value="1">Mall</option>
                        <option value="2">Perkantoran</option>
                        <option value="3">Pasar Tradisional (Wirausaha)</option>
                        <option value="4">Transportation Hub</option>
                    </select>
                </div>
{{--                <div class='form-group'>--}}
{{--                    <label>Dari Jenis Nama Merchant</label>--}}
{{--                    <select name="merchant_type_of_name" class="form-control"></select>--}}
{{--                </div>--}}
                <div class='form-group'>
                    <label>Kode Provinsi</label>
                    <select class="form-control" name="prov_code" id="">
                    </select>
                </div>
                <div class='form-group'>
                    <label>Kode Kabupaten</label>
                    <select class="form-control" name="city_code" class="" id="">
                    </select>
                </div>
                <div class='form-group'>
                    <label>Kode Kecamatan</label>
                    <select class="form-control" name="district_code" id="">
                    </select>
                </div>
                <div class='form-group'>
                    <label>Kode Kelurahan</label>
                    <select class="form-control" name="subdistrict_code" id="">
                    </select>
                </div>
            </form>
        </div>
        <div class='panel-footer'>
            <input type='submit' class='btn btn-primary' value='Save changes' form="form"/>
        </div>
    </div>
@endsection
@push('bottom')
    <script defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVtqqM-WDHZKV9ffLB-RWkUg1mTUEgJxY&callback=initMap&language=id&libraries=places"></script>
    <script src="{{asset('lib/select2/select2.min.js')}}"></script>
    <script src="{{asset('js/admin/add_merchant.js')}}"></script>
    <script>
        document.addEventListener('readystatechange', function(event) {
            if (event.target.readyState === "complete") {
                bussinessTypeSelect.val({{$row->bussiness_type_id}}).trigger("change");
                merchantTypeSelect.val({{$row->merchant_type ?: 0}}).trigger("change");
                merchantTypeOfSelect.val({{$row->merchant_type_of ?: 0}}).trigger("change");
                provinceSelect.val({{$row->prov_code}}).trigger("change");
                citySelect.val("{{$row->city_code}}").trigger("change");
                districtSelect.val("{{$row->district_code}}").trigger("change");
                subdistrictSelect.val("{{$row->subdistrict_code}}").trigger("change");
                marker.setPosition(new google.maps.LatLng({{$row->latitude}}, {{$row->longitude}}))
                map.setCenter(new google.maps.LatLng({{$row->latitude}}, {{$row->longitude}}))
            }
        });
    </script>
@endpush
