@extends('crudbooster::admin_template')
@section('content')
<div class='table-responsive'>
    <table id='table-detail' class='table table-striped'>
        <tr>
            <td>NIK</td>
            <td>{{$row->nik}}</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{$row->name}}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td><a href='mailto:{{$row->email}}' target="_blank">{{$row->email}}</a></td>
        </tr>
        <tr>
            <td>Address</td>
            <td>{{$row->address}}</td>
        </tr>
        <tr>
            <td>Phone</td>
            <td>{{$row->phone}}</td>
        </tr>
        <tr>
            <td>Selfie</td>
            <td><img src="{{asset($row->selfie)}}" alt=""></td>
        </tr>
        <tr>
            <td>Birth</td>
            <td>{{$row->birth}}</td>
        </tr>
        <tr>
            <td>Gender</td>
            <td>{{$row->gender}}</td>
        </tr>
        <tr>
            <td>Religion</td>
            <td>{{$row->religion}}</td>
        </tr>
        <tr>
            <td>Nationality</td>
            <td>{{$row->nationality}}</td>
        <tr>
            <td>Marital Status</td>
            <td>{{$row->marital_status}}</td>
        </tr>
        <tr>
            <td>Last Login</td>
            <td>{{$row->last_login}}</td>
        </tr>
        <tr>
            <td>Regid</td>
            <td>{{$row->regid}}</td>
        </tr>
        <tr>
            <td>Secret</td>
            <td>{{$row->secret}}</td>
        </tr>
        <tr>
            <td>Qrcode</td>
            <td><img style="max-width: 200px; height: 200px" src="{{asset($row->qrcode)}}" alt=""></td>
        </tr>
    </table>
</div>
@endsection
