<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@push('head')
    <link rel="stylesheet" href="{{asset('lib/select2/select2.min.css')}}">
@endpush
@section('content')
    <!-- Your html goes here -->
    <div class='panel panel-default'>
        <div class='panel-heading'>Edit Form</div>
        <form method='post' action='{{CRUDBooster::mainpath('edit-save/'.$row->id)}}'>
            {{csrf_field()}}
            <div class='panel-body'>
                <div class='form-group'>
                    <label>Nama</label>
                    <input type='text' name='name' required class='form-control' value='{{$row->name}}'/>
                </div>
                <div class='form-group'>
                    <label>Kode Kabupaten</label>
                    <select name='city_code' class='form-control'>
                        <option value="{{$row->city_code}}" selected>
                            {{$row->city_name}}
                        </option>
                    </select>
                </div>
                <div class='form-group'>
                    <label>Kode Kecamatan</label>
                    <select name='district_code' class="form-control" id="">
                        <option value="{{$row->district_code}}" selected>
                            {{$row->distrcit_name}}
                        </option>
                    </select>
                </div>
                <div class='form-group'>
                    <label>Alamat</label>
                    <input type='text' name='address' required class='form-control' value='{{$row->address}}'/>
                </div>
                <div class='form-group'>
                    <label>Phone</label>
                    <input type='text' name='phone' required class='form-control' value='{{$row->phone}}'/>
                </div>
            </div>
            <div class='panel-footer'>
                <input type='submit' class='btn btn-primary' value='Save changes'/>
            </div>
        </form>

    </div>
@endsection
@push('bottom')
    <script src="{{asset('lib/select2/select2.min.js')}}"></script>
    <script src="{{asset('js/admin/health_center.js')}}"></script>
@endpush
