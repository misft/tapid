@extends('crudbooster::admin_template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <div class="border-box">
                    <div class="small-box bg-blue">
                        <div class="inner inner-box">
                            <h3>{{ $box['capacity']->capacity }}</h3>
                            <h4>Jumlah Kapasitas Normal</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-people"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="border-box">
                    <div class="small-box bg-yellow">
                        <div class="inner inner-box">
                            <h3>{{ intval($box['capacity']->capacity * ($box['capacity']->capacity_percentage / 100)) }}</h3>
                            <h4>Jumlah Kapasitas PSBB</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-people"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="border-box">
                    <div class="small-box
                        {{ $box['visitor_count'] > intval($box['capacity']->capacity * $box['capacity']->capacity_percentage) ? "bg-red" : "" }}
                        {{ $box['visitor_count'] == intval($box['capacity']->capacity * $box['capacity']->capacity_percentage) ? "bg-yellow" : ""  }}
                        {{ $box['visitor_count'] < intval($box['capacity']->capacity * $box['capacity']->capacity_percentage) ? "bg-green" : ""  }}">
                        <div class="inner inner-box">
                            <h3>
                                {{ $box['visitor_count'] > intval($box['capacity']->capacity * $box['capacity']->capacity_percentage) ? "Over Capacity" : "" }}
                                {{ $box['visitor_count'] == intval($box['capacity']->capacity * $box['capacity']->capacity_percentage) ? "Warning" : ""  }}
                                {{ $box['visitor_count'] < intval($box['capacity']->capacity * $box['capacity']->capacity_percentage) ? "Safe" : ""  }}
                            </h3>
                            <h4>Jumlah Kunjungan saat ini</h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-people"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <h3>Riwayat Over Kapasitas</h3>
                <a href="admin/download/overcapacity_history">Print</a>
                <table class="table table-responsive table-striped table-danger">
                    <thead class="bg-fuchsia">
                    <tr>
                        <th>Tanggal</th>
                        <th>Jam</th>
                        <th>Durasi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($table['overcapacity_history'] as $i)
                        <tr>
                            <td>{{ CRUDBooster::parseDateIndo($i->start_at) }}</td>
                            <td>{{ date('H:i:s', strtotime($i->start_at)) }} - {{ date('H:i:s', strtotime($i->end_at)) }}</td>
                            <td>
                                @php
                                    $now = new DateTime($i->start_at);
                                    $then = new DateTime($i->end_at);
                                    $diff = date_diff($then, $now, true);
                                @endphp
                                {{  $diff->format('%h jam %i menit %s detik') }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-xs-12">
                <h3>Kunjungan Terakhir</h3>
                <canvas id="visitor_count_currently" style="width: 100%; height: 300px"></canvas>
            </div>
        </div>
    </div>
@endsection

@push('bottom')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

    <script>
        var visitorCountRecently = document.getElementById('visitor_count_currently').getContext("2d");
        var responseVisitorRecently = {!! $chart['visitor_recently'] !!};
        var response

        function getDataset() {
            var arr = [
                {
                    label: "Visitor",
                    backgroundColor: "blue",
                    data: [],
                }
            ]
            responseVisitorRecently.forEach(e => {
                arr[0].data.push(e.visitor_count);
            });
            return arr;
        }

        function getLabel() {
            var arr = [];
            responseVisitorRecently.forEach(e => {
                arr.push(e.check_in);
            });
            return arr;
        }

        var data = {
            labels: getLabel(),
            datasets: getDataset()
        }
        var chartVisitorCountRecently = new Chart(visitorCountRecently, {
            type: 'line',
            data: data,
            options: {
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                                stepSize: 1,
                            }
                        }
                    ]
                }
            }
        })
    </script>
@endpush
