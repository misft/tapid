@extends('crudbooster::admin_template')
@push('head')
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
    <style>
        #preview {
            width: 100%;
            height: 100%;
            max-height: 100vh;
        }
    </style>
@endpush
@section('content')
    <video id="preview"></video>
    <button class="btn btn-light">Switch camera</button>
@endsection
@push('bottom')
    <script type="text/javascript">
        function getCurrentLink() {
            if(window.location.hostname == "localhost" || "tapid.co.id") {
                return "/"
            }
            else {
                return "/tapid/public"
            }
        }
        var scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 2, mirror: false });
        scanner.addListener('scan',function(content){
            $.ajax(getCurrentLink()+'api/check_in_merchant', {
                method: 'POST',
                data: {
                    secret: content,
                    id_merchant: {{ CRUDBooster::myId() }}
                }
            }).then(res => {
                if(res.api_status == 1) {
                    alert("Check in berhasil");
                }
                if(res.api_status == 0) {
                    alert(res.api_message);
                }
            })
        });
        var cameraIndex = 1;
        Instascan.Camera.getCameras().then(function (cameras){
            console.log(cameras);
            if(cameras.length>0){
                try  {
                    scanner.start(cameras[cameraIndex]);
                }
                catch(err) {
                    cameraIndex = 0;
                    scanner.start(cameras[cameraIndex]);
                }
            }else{
                console.error('No cameras found.');
                alert('No cameras found.');
            }
        }).catch(function(e){
            console.error(e);
            alert(e);
        });

        document.querySelector("button").addEventListener("click", function() {
            Instascan.Camera.getCameras().then(function (cameras){
                if(cameras.length>0){
                    if(cameraIndex == 0) {
                        cameraIndex++
                        scanner.start(cameras[cameraIndex])
                    }
                    else {
                        cameraIndex = 0
                        scanner.start(cameras[cameraIndex])
                    }
                }
            }).catch(function(e){
                console.error(e);
                alert(e);
            });
        })
    </script>
@endpush
