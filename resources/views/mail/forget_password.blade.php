<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kamu telah diverifikasi</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap');

        .row {
            display: flex;
            flex-wrap: wrap;
        }

        .justify-content-center {
            justify-content: center;
        }

        .w-100 {
            width: 100%;
        }

        .text-center {
            text-align: center;
        }

        .img-fluid {
            max-width: 100%;
        }

        p {
            font-family: "Inter", sans-serif;
            font-weight: 400;
            color: #0a0a0a;
        }

        strong {
            font-family: "Inter-Bold", sans-serif;
            font-weight: 700;
            color: #0a0a0a;
        }
    </style>
</head>
<body class="row justify-content-center" style="margin-top: 10px">
<div class="w-100">
    <div class="w-100 text-center">
        <img src="{{$image}}" class="img-fluid mx-auto" style="height: 200px" alt="">
    </div>
    <div class="w-100 text-center" style="margin-bottom: 25px">
        <strong>Hai {{$name}}, kamu lupa kata sandimu?</strong>
        <p>Untuk mengatur ulang kata sandimu silahkan klik link dibawah ini:</p>
    </div>
    <div class="w-100 text-center" style="margin-top: 25px; margin-bottom: 25px">
        <a href="{{$link}}">
            {{$link}}
        </a>
    </div>
    <div class="w-100 text-center" style="margin-top: 25px">
        <p>Salam hangat, TapID</p>
    </div>
    <div class="w-100 text-center" style="margin-top: 32px">
        <p>
            Copyright © 2020 Tap.id All Rights Reserved
        </p>
    </div>
</div>
</body>
</html>
