<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{asset('images/ic_ios_launcher-min.png')}}" type="image/png">
    <title>TapID | Tracing Corona</title>
    <link rel="stylesheet" href="{{asset('css/index.css')}}">
    <style>
        .logo {
            position: absolute;
            max-width: 150px;
        }

        .phone-desktop {
            position: absolute;
        }
    </style>
</head>
<body style="background-color: #F3363F; width: 100%; max-width: 100%;;height: 100vh; margin: 0">
<img class="logo" src="{{asset('images/ic_ios_launcher-min.png')}}" alt="">
<a href="/admin" onclick="void(0)" style="text-decoration: none; font-family: 'Inter'; color: whitesmoke; position: absolute; right: 0; margin-right: 25px; margin-top: 25px;">Login Merchant</a>
<div class="title-text">
    <p>Aplikasi Tracing Covid-19 untuk<br>mengetahui cluster baru dengan cepat</p>
    <div class="d-flex">
        <a href="">
            <img class="app-store" style="margin-right: 5px"
                 src="{{asset('images/1200px-Google_Play_Store_badge_EN.svg.png')}}" alt="">
        </a>
        <a href="">
            <img class="app-store" style="margin-left: 5px" src="{{asset('images/440px-Download_on_iTunes.svg.png')}}"
                 alt="">
        </a>
    </div>
</div>
<img src="{{asset('images/mockup_tapid-min.png')}}" class="image-phone" alt="">
<footer style="padding-bottom: 25px; display: flex; flex-direction: column; width: 100%; justify-content: center">
    <div style="text-align: center">
    <p>&copy; Copyright 2020; All Rights Reserved</p>
    </div>
    <div style="display: flex; flex-direction: row; width: 100%; text-align: center; justify-content: center">
        <a href="/privacy_policy" target="_blank" onclick="void(0)" style="text-decoration: none; font-family: 'Inter'; color: whitesmoke">Privacy
            Policy |</a>
        <a href="/terms_of_service" target="_blank" onclick="void(0)" style="text-decoration: none; font-family: 'Inter'; color: whitesmoke">&nbsp;Terms of Service</a>
    </div>
</footer>
</body>
</html>
