<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

<!-- Bootstrap 3.4.1 JS -->
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/dist/js/app.js') }}" type="text/javascript"></script>

<!--BOOTSTRAP DATEPICKER-->
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<link rel="stylesheet" href="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/datepicker/datepicker3.css') }}">

<!--BOOTSTRAP DATERANGEPICKER 2.1.27 AND MOMENT 2.13.0 -->
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<link rel="stylesheet" href="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/daterangepicker/daterangepicker-bs3.css') }}">

<!-- Bootstrap time Picker -->
<link rel="stylesheet" href="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

<link rel='stylesheet' href='{{ asset("vendor/crudbooster/assets/lightbox/dist/css/lightbox.min.css") }}'/>
<script src="{{ asset('vendor/crudbooster/assets/lightbox/dist/js/lightbox.min.js') }}"></script>

<!--SWEET ALERT-->
<script src="{{asset('vendor/crudbooster/assets/sweetalert/dist/sweetalert.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('vendor/crudbooster/assets/sweetalert/dist/sweetalert.css')}}">

<!--MONEY FORMAT-->
<script src="{{asset('vendor/crudbooster/jquery.price_format.2.0.min.js')}}"></script>

<!--DATATABLE-->
<link rel="stylesheet" href="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/datatables/dataTables.bootstrap.css')}}">
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset ('vendor/crudbooster/assets/adminlte/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script>
    var ASSET_URL = "{{asset('/')}}";
    var APP_NAME = "{{Session::get('appname')}}";
    var ADMIN_PATH = '{{url(config("crudbooster.ADMIN_PATH")) }}';
    var NOTIFICATION_JSON = "{{route('NotificationsControllerGetLatestJson')}}";
    var NOTIFICATION_INDEX = "{{route('NotificationsControllerGetIndex')}}";

    var NOTIFICATION_YOU_HAVE = "{{trans('crudbooster.notification_you_have')}}";
    var NOTIFICATION_NOTIFICATIONS = "{{trans('crudbooster.notification_notification')}}";
    var NOTIFICATION_NEW = "{{trans('crudbooster.notification_new')}}";

    $(function () {
        $('.datatables-simple').DataTable();
    })
</script>
<script src="{{asset('vendor/crudbooster/assets/js/main.js').'?r='.time()}}"></script>
<script src="{{asset('firebase.js')}}"></script>
<script src="{{asset('firebase-messaging.js')}}"></script>
<script>
    let myId = {{CRUDBooster::myId()}};
    const firebaseConfig = {
        apiKey: "AIzaSyAag-JGRX__4feGTRdWkekdCYe65JqGO0Q",
        authDomain: "neonik-33c0d.firebaseapp.com",
        databaseURL: "https://neonik-33c0d.firebaseio.com",
        projectId: "neonik-33c0d",
        storageBucket: "neonik-33c0d.appspot.com",
        messagingSenderId: "966676773187",
        appId: "1:966676773187:web:da1279b5ad4f91c0d057f6",
        measurementId: "G-9R715L8PPM"
    };
    firebase.initializeApp(firebaseConfig);

    let messaging = firebase.messaging();
    messaging.requestPermission()
    Notification.requestPermission()
    messaging.usePublicVapidKey("BC-W5mOiOjj0nvmEeELYpvbVcYZUfIjZkvPu29eBs20FfSPySroD355PPOSu0qG_GltO1bPCh9Oz93cGevZvxY0")
    messaging.getToken().then((currentToken) => {
        console.log("Token received")
        if (currentToken) {
            $.ajax("/api/store_regid", {
                method: 'POST',
                data: {
                    id: myId,
                    regid: currentToken
                },
                success: function (e) {
                    console.log(e);
                }
            }).then((e) => {
                console.log(e);
            })
        } else {
            console.log('No Instance ID token available. Request permission to generate one.');
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        showToken('Error retrieving Instance ID token. ', err);
    });

    messaging.onTokenRefresh(() => {
        messaging.getToken().then((refreshedToken) => {
            $.ajax("/api/store_regid", {
                method: 'POST',
                data: {
                    id: myId,
                    regid: refreshedToken
                },
                succes: function (e) {
                    console.log(e);
                }
            })
        }).catch((err) => {
            console.log('Unable to retrieve refreshed token ', err);
        });
    });

    messaging.onMessage((payload) => {
        console.log('[firebase-messaging-sw.js] Received background message ', payload);
        // Customize notification here
        const notificationTitle = 'TapID';
        const notificationOptions = {
            body: payload.notification.title,
            content: payload.notification.body,
            icon: '{{asset('images/ic_ios_launcher-min.png')}}'
        };
        const notification = new Notification(notificationTitle, notificationOptions);
        notification.onclick = function() {
            window.open("https://tapid.co.id/admin");
        }

        return notification;
    });
</script>
<script>
    navigator.serviceWorker.register('{{asset('firebase-messaging-sw.js')}}')
</script>
