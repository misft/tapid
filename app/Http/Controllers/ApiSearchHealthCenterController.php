<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		//Model
        use App\HealthCenter;

		class ApiSearchHealthCenterController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $healthcenter_repo;

		    function __construct() {
				$this->table       = "health_center";
				$this->permalink   = "search_health_center";
				$this->method_type = "get";

				//Init repo
                $this->healthcenter_repo = new \App\Repositories\HealthCenterRepository(new HealthCenter());
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $postdata = CRUDBooster::valid([
                    'name'=>'required'
                ]);

                $name = $postdata['name'];

                $data = $this->healthcenter_repo->getWithName($name);

                $postdata['api_status'] = 1;
                $postdata['api_message'] = "success";
                $postdata['data'] = $data->items();
                $postdata['current_page'] = $data->currentPage();
                $postdata['last_page'] = $data->lastPage();

                return response()->json($postdata)->send();

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
