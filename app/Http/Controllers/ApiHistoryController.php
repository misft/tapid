<?php namespace App\Http\Controllers;

		use App\Repositories\HistoryRepository;
        use App\Repositories\UsersRepository;
        use Session;
		use Request;
		use DB;
		use CRUDBooster;

		//Model
        use App\Users;
        use App\Notifications;
        use App\History;

		class ApiHistoryController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $users_repo;
		    protected $history_repo;

		    function __construct() {
				$this->table       = "history";
				$this->permalink   = "history";
				$this->method_type = "get";

				//Init repo
                $this->users_repo = new UsersRepository(New Users());
                $this->history_repo = new HistoryRepository(new History());
            }


		    public function hook_before(&$postdata) {
				//Validate request
                $postdata = CRUDBooster::valid([
                    'id'=>'required'
                ]);

                //Get user data
                $user = $this->users_repo->find($postdata['id']);

                //Get NIK
                $nik = $user->nik;

				//Get from database and parse response
                $data = $this->history_repo->getAllHistories($postdata['id'], $nik);

                //Send response
				$postdata['api_status'] = 1;
				$postdata['api_message'] = 'success';
				$postdata['is_verified'] = $user->is_verified;
				$postdata['data'] = $data->items();
				$postdata['current_page'] = $data->currentPage();
				$postdata['last_page'] = $data->lastPage();

				return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
				exit;
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
