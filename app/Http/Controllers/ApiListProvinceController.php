<?php namespace App\Http\Controllers;

use CRUDBooster;
use DB;
use Request;
use Session;

class ApiListProvinceController extends \crocodicstudio\crudbooster\controllers\ApiController
{

    function __construct()
    {
        $this->table = "province";
        $this->permalink = "list_province";
        $this->method_type = "get";
    }


    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
        $name = g('name');

        $postdata['api_status'] = 1;
        $postdata['api_message'] = 'success';
        if (empty($name)) {
            $postdata['data'] = DB::table('province')->get();
        } else {
            $postdata['data'] = DB::table('province')
                ->where('name', 'like', "%$name%")
                ->get();
        }


        return response()->json($postdata)->send();
    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query

    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process

    }

}
