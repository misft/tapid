<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiVisitorCounterController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "history";        
				$this->permalink   = "visitor_counter";    
				$this->method_type = "get";    
		    }
		

		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
				$data = DB::table('history')
					->where('id_merchant', g('id_merchant'))
					->whereNull('out')
					->whereNotNull('in')
					->count();
				return response()->json([
					'api_status'=>1,
					'api_message'=>'success',
					'data'=>$data
				])->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}