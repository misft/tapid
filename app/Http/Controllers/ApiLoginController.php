<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;
		use Hash;
		use Validator;

		//Model
		use App\Users;
		use App\History;
		use App\Merchants;

		class ApiLoginController extends \crocodicstudio\crudbooster\controllers\ApiController {

            protected $users_repo;
            protected $history_repo;
            protected $merchants_repo;

            function __construct() {
				$this->table       = "users";
				$this->permalink   = "login";
				$this->method_type = "post";

				//Init repo
                $this->users_repo = new \App\Repositories\UsersRepository(new Users());
                $this->history_repo = new \App\Repositories\HistoryRepository(new History());
                $this->merchants_repo = new \App\Repositories\MerchantsRepository(new Merchants());
		    }

		    public function hook_before(&$postdata) {

		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
		    }

		    public function hook_after($postdata,&$result) {
				//Validate data
				$postdata = CRUDBooster::valid([
					'phone'=>'required|max:15',
					'password'=>'required|max:16',
					'regid'=>'required'
				], [
					'required'=>':Attribute harus diisi',
				]);

				//Check if user exist and get it
				$user = $this->users_repo->getLoginUser($postdata['phone']);

				//If user not exist
				if(empty($user)) {
				    return response()->json([
				        'api_status'=>0,
                        'api_message'=>'Akun anda belum terdaftar'
                    ])->send();
                }

				//Check password correct
				$is_auth = Hash::check($postdata['password'], $user->password);
				if(!$is_auth) {
					return response()->json([
						'api_status'=>0,
						'api_message'=>'Password atau nomer anda salah'
					])->send();
				}

				//Update data after login
                $this->users_repo->updateAfterLogin($postdata['phone'], $postdata['regid']);

				//Prepare data and response
				$latest_checkin = $this->history_repo->userLastCheckIn($user->id);
				$user->id_checkin = ($latest_checkin == null) ? 0 : $latest_checkin;
				$user->is_checkin = ($latest_checkin == null) ? 0 : 1;
				$user->regid = $postdata['regid'];
				$user->merchant_name = $this->merchants_repo->findMerchantNameByIdHistory($latest_checkin);
				unset($user->password);

				//Send response
				$result['api_status'] = 1;
				$result['api_message'] = 'success';
				$result['data'] = $user;
				return response()->json($result)->send();
			}

		}
