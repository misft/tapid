<?php namespace App\Http\Controllers;

        use Illuminate\Contracts\Encryption\DecryptException;
        use Session;
		use Request;
		use DB;
		use CRUDBooster;
		use Hash;

		//Model
        use App\Users;

		class ApiResetPasswordController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $users_repo;

		    function __construct() {
				$this->table       = "users";
				$this->permalink   = "reset_password";
				$this->method_type = "post";

				//Init repo
                $this->users_repo = new \App\Repositories\UsersRepository(new Users());
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $postdata = CRUDBooster::valid([
                    'secret'=>'required',
                    'password'=>'required',
                    'confirmation_password'=>'required'
                ]);

                try{
                    $email = decrypt($postdata['secret']);
                }
                catch (DecryptException $e) {
                    return response()->json([
                        'api_status'=>0,
                        'api_message'=>"Secret tidak valid"
                    ])->send();
                }

                //Check if record exist
                $is_request_exist = DB::table('forget_password')
                    ->where('encrypted', $postdata['secret'])
                    ->exists();
                if(!$is_request_exist) {
                    return response()->json([
                        'api_status'=>0,
                        'api_message'=>'Secret tidak ditemukan'
                    ])->send();
                }

                //Check expiry
                $is_expired = DB::table('forget_password')
                    ->where('encrypted', $postdata['secret'])
                    ->where('expired_at', '<', date('Y-m-d H:i:s'))
                    ->exists();
                if($is_expired) {
                    return response()->json([
                        'api_status'=>0,
                        'api_message'=>'Maaf, token ini sudah habis'
                    ])->send();
                }

                $user = $this->users_repo->findWithEmail($email);

                //Check if password match
                if($postdata['password'] != $postdata['confirmation_password']) {
                    return response()->json([
                        'api_status'=>0,
                        'api_message'=>"Password tidak match"
                    ])->send();
                }

                //Check if password is formatted or not
                $password = CRUDBooster::validatePassword($postdata['password']);
                if(empty($user)) {
                    return response()->json([
                        'api_status'=>0,
                        'api_message'=>'Pengguna tidak ditemukan'
                    ])->send();
                }

                $this->users_repo->updatePasswordWithEmail($email, $password);

                DB::table('forget_password')
                    ->where('encrypted', $postdata['secret'])
                    ->update([
                        'expired_at'=>date('Y-m-d H:i:s')
                    ]);

                return response()->json([
                    'api_status'=>1,
                    'api_message'=>'Akun anda telah diperbarui. Silahkan login!'
                ])->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
                exit;
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
                exit;
		    }

		}
