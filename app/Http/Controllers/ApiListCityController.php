<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiListCityController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "city";
				$this->permalink   = "list_city";
				$this->method_type = "get";
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $code = g('code');
                $name = g('name');

                $postdata['api_status'] = 1;
                $postdata['api_message'] = 'success';
                if($code) {
                    $postdata['data'] = DB::table('city')
                        ->whereRaw("`code` REGEXP '^[0-9]{2}[,.][0-9]{2}$'")
                        ->where('code', 'like', "$code%")
                        ->get();
                }
                else if($name) {
                    $postdata['data'] = DB::table('city')
                        ->where('name', 'like', "%$name%")
                        ->get();
                }
                else {
                    $postdata['data'] = DB::table('city')->get();
                }

                return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
