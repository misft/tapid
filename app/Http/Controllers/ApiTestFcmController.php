<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiTestFcmController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "users";        
				$this->permalink   = "test_fcm";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
				$regid = g('regid');
				if(!is_array($regid)) {
					$regid = array($regid);
				}
				
				$fcm = CRUDBooster::sendFCM($regid,[
					'title'=>'Hello',
					'content'=>'Test',
					'date'=>'2020-01-01 00:00:00',
					'id_check_in'=>1,
					'id_check_out'=>1
				]);

				return response($fcm, 200)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}