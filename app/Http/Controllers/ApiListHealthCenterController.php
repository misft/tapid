<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiListHealthCenterController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "health_center";
				$this->permalink   = "list_health_center";
				$this->method_type = "get";
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $district_code = g('district_code');
                $name = g('name');

                $postdata['api_status'] = 1;
                $postdata['api_message'] = 'success';

                if(!empty($district_code) && !empty($name)) {
                    $data = DB::table('health_center')
                        ->where('district_code', 'like', "%$district_code%")
                        ->where('health_center.name', 'like', "%$name%")
                        ->leftJoin('district', 'health_center.district_code', 'district.code')
                        ->select('health_center.*', 'district.name as district_name')
                        ->paginate(100);
                    foreach($data->items() as $i) {
                        $i->district_name = "Kecamatan ".$i->district_name;
                    }
                    $postdata['data'] = $data->items();
                    $postdata['current_page'] = $data->currentPage();
                    $postdata['last_page'] = $data->lastPage();
                }
                else if (!empty($district_code)) {
                    $data = DB::table('health_center')->where('district_code', 'like', "%$district_code%")
                        ->leftJoin('district', 'health_center.district_code', 'district.code')
                        ->select('health_center.*', 'district.name as district_name')
                        ->paginate(100);
                    foreach($data->items() as $i) {
                        $i->district_name = "Kecamatan ".$i->district_name;
                    }
                    $postdata['data'] = $data->items();
                    $postdata['current_page'] = $data->currentPage();
                    $postdata['last_page'] = $data->lastPage();
                }
                else if(!empty($name)) {
                    $data = DB::table('health_center')
                        ->where('health_center.name', 'like', "%$name%")
                        ->leftJoin('district', 'health_center.district_code', 'district.code')
                        ->select('health_center.*', 'district.name as district_name')
                        ->paginate(100);
                    foreach($data->items() as $i) {
                        $i->district_name = "Kecamatan ".$i->district_name;
                    }
                    $postdata['data'] = $data->items();
                    $postdata['current_page'] = $data->currentPage();
                    $postdata['last_page'] = $data->lastPage();
                }
                else {
                    $data = DB::table('health_center')
                        ->leftJoin('district', 'health_center.district_code', 'district.code')
                        ->select('health_center.*', 'district.name as district_name')
                        ->paginate(100);
                    foreach($data->items() as $i) {
                        $i->district_name = "Kecamatan ".$i->district_name;
                    }
                    $postdata['data'] = $data->items();
                    $postdata['current_page'] = $data->currentPage();
                    $postdata['last_page'] = $data->lastPage();

                }

                return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
                exit;
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
                exit;
		    }

		}
