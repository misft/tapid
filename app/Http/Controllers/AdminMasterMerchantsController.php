<?php namespace App\Http\Controllers;

	use Log;
	use Hash;
    use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminMasterMerchantsController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "name";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = true;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = false;
			$this->table = "cms_users";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nama","name"=>"name"];
            $this->col[] = ["label"=>"Alamat","name"=>"address"];
			$this->col[] = ["label"=>"Latitude","name"=>"latitude"];
			$this->col[] = ["label"=>"Longitude","name"=>"longitude"];
			$this->col[] = ["label"=>"Kapasitas","name"=>"capacity"];
            $this->col[] = ["label"=>"Persentase Kapasitas PSBB","name"=>"bussiness_type_id", "callback"=>function($row) {
                $capacity_percentage = DB::table('bussiness_type')->where('id', $row->bussiness_type_id)->first()->capacity_percentage;
                return $capacity_percentage."%";
            }];
            $this->col[] = ["label"=>"Kapasitas PSBB","name"=>"capacity", "callback"=>function($row) {
                $capacity_percentage = DB::table('bussiness_type')->where('id', $row->bussiness_type_id)->first()->capacity_percentage;
                return intval($row->capacity * ($capacity_percentage / 100));
            }];
            $this->col[] = ["label"=>"Pengunjung","name"=>"visitor_count"];
            $this->col[] = ["label"=>"QRCode","name"=>"qrcode","image"=>true];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Password','name'=>'password','type'=>'text','validation'=>'required|string|min:6|max:16','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Confirmation Password','name'=>'confirmation_password','type'=>'text','validation'=>'required|string|min:6|max:16','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'Address','name'=>'address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Latitude','name'=>'latitude','type'=>'hidden','validation'=>'required','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Longitude','name'=>'longitude','type'=>'hidden','validation'=>'required','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Capacity','name'=>'capacity','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10', 'helper'=>'Masukkan jumlah kapasitas normal sebelum pandemi'];
            $this->form[] = ['label'=>'Capacity Percentage','name'=>'capacity_percentage','type'=>'text','validation'=>'required','width'=>'col-sm-10'];
            $this->form[] = ['label'=>'Qrcode','name'=>'qrcode','type'=>'text','validation'=>'required','width'=>'col-sm-10'];
            $this->form[] = ['label'=>'Merchant Type','name'=>'merchant_type','type'=>'text','width'=>'col-sm-10'];
            $this->form[] = ['label'=>'Merchant Type Of','name'=>'merchant_type_of','type'=>'text','width'=>'col-sm-10'];
            $this->form[] = ['label'=>'Jenis Bisnis','name'=>'bussiness_type_id','type'=>'text','width'=>'col-sm-10'];
            # END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Name','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			//$this->form[] = ['label'=>'Address','name'=>'address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Latitude','name'=>'latitude','type'=>'hidden','validation'=>'required','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Longitude','name'=>'longitude','type'=>'hidden','validation'=>'required','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Capacity','name'=>'capacity','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Qrcode','name'=>'qrcode','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Separate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
			//Your code here
			$query->where('id_cms_privileges', '!=', 1);
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
			//Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        $postdata = Request::all();
	        unset($postdata['_token']);
			unset($postdata['confirmation_password']);

			$postdata['id_cms_privileges'] = 2;
			$postdata['status'] = 'Active';
			$postdata['password'] = Hash::make($postdata['password']);
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
            $merchant = DB::table('cms_users')->where('id', $id)->first();

            if($merchant->merchant_type == 0) {
                exit;
            }
            $merchant->prov_name = DB::table('master_place')->where('code', $merchant->prov_code)->first()->name;
            $merchant->city_name = DB::table('master_place')->where('code', $merchant->city_code)->first()->name;
            $merchant->district_name = DB::table('master_place')->where('code', $merchant->district_code)->first()->name;
            $merchant->subdistrict_name = DB::table('master_place')->where('code', $merchant->subdistrict_code)->first()->name;
			$qr = CRUDBooster::generateQR([
				'id'=>$merchant->id,
				'name'=>$merchant->name
			]);
			DB::table('cms_users')->where('id', $id)->update([
				'qrcode'=>CRUDBooster::uploadBase64(base64_encode($qr))
			]);

            $ctrace_url = env("C-TRACE_DEVELOPMENT");
            $api_key = env("C-TRACE_API_KEY");

            //Check if merchant existed in third party
            $curl = curl_init("$ctrace_url/api/get/ListtenantJSC");
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                "api_token: bearer $api_key"
            ]);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPGET, true);
            $list_merchant = curl_exec($curl);
            curl_close($curl);

            $response = json_decode($list_merchant);

            if($response->resp_code == 0) {
                return CRUDBooster::redirect('admin/master_merchants', trans("crudbooster.alert_add_data_success"), 'success');
                exit;
            }

            $exist = 0;
            $tenant_code = null;
            foreach($response->data as $i) {
                $tenant_code = intval($i->code_tenant_jsc + 1);
                if($i->tenant_name == $merchant->name) {
                    $exist = 1;
                    break;
                }
            }

            if($exist == 0) {
                return CRUDBooster::redirect('admin/master_merchants', trans("crudbooster.alert_add_data_success"), 'success');
            }

            $curl = curl_init("$ctrace_url/api/reg/merchant");
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                "api_token: bearer $api_key"
            ]);
            $reg_request = [
                'act'=>'register_merchant',
                'nama_tenant'=>$merchant->name,
                'dibuat_oleh'=>"TapID",
                'x'=>$merchant->latitude,
                'y'=>$merchant->longitude,
                'dari_tipe_tempat'=>$merchant->merchant_type_of,
                'tenant_code_jsc'=>$tenant_code,
                'kode_prov'=>$merchant->prov_code,
                'nama_prov'=>$merchant->prov_name,
                'kode_kab'=>$merchant->city_code,
                'nama_kab'=>$merchant->city_name,
                'kode_kec'=>$merchant->district_code,
                'nama_kec'=>$merchant->district_name,
                'kode_kel'=>$merchant->subdistrict_code,
                'nama_kel'=>$merchant->subdistrict_name,
                'kapasitas_gedung'=>$merchant->capacity,
                'jumlah_lantai'=>$merchant->floor_count
            ];
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($reg_request));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            $result = curl_exec($curl);
            curl_close($curl);

            DB::table('cms_users')->where('id', $id)->update([
                'jsc_code'=>$tenant_code
            ]);

            return CRUDBooster::redirect('admin/master_merchants', trans("crudbooster.alert_add_data_success"), 'success');
        }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here
            $postdata = Request::all();
            unset($postdata['_token']);
            unset($postdata['confirmation_password']);
            unset($postdata['qrcode']);
            if($postdata['password'] == null || $postdata['password'] == "") {
                unset($postdata['password']);
            }
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



		public function getAdd() {
			if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {
				CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = [];
			$data['page_title'] = 'Add Data';

			//Please use cbView method instead view method from laravel
			$this->cbView('admin/add_merchant',$data);
		}

		public function getEdit($id) {
			//Create an Auth
			if(!CRUDBooster::isUpdate() && $this->global_privilege==FALSE || $this->button_edit==FALSE) {
			  CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
			}

			$data = [];
			$data['page_title'] = 'Edit Data';
			$data['row'] = DB::table('cms_users')->where('id',$id)->first();
            $data['prov_code'] = DB::table('master_place')->whereRaw("`code` REGEXP '^[0-9]{2}$'")->get();
            $data['city_code'] = DB::table('master_place')->whereRaw("`code` REGEXP '^[0-9]{2}[,.][0-9]{2}$'")->get();
            $data['district_code'] = DB::table('master_place')->whereRaw("`code` REGEXP '^[0-9]{2}[,.][0-9]{2}[,.][0-9]{2}$'")->get();
            $data['subdistrict_code'] = DB::table('master_place')->whereRaw("`code` REGEXP '^[0-9]{2}[,.][0-9]{2}[,.][0-9]{2}[,.][0-9]{4}$'")->get();

			//Please use cbView method instead view method from laravel
			$this->cbView('admin/edit_merchant', $data);
		  }

	}
