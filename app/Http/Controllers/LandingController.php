<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LandingController extends Controller
{
    public function index() {
        return view('index');
    }

    public function privacyPolicyPage() {
        $data['data'] = DB::table('privacy_policy')->first();

        return view('privacy_policy', $data);
    }

    public function termsOfServicePage() {
        $data['data'] = DB::table('terms_and_services')->first();

        return view('privacy_policy', $data);
    }
}
