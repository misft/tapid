<?php

namespace App\Http\Controllers;

use CRUDBooster;
use DB;
use Illuminate\Http\Request;
use PDO;

class TestController extends Controller
{
    public function test(Request $req)
    {
        if(empty($req->get('id_user')) || empty($req->get('secret'))) {
            return response([
                'api_status' => 0,
                'api_message' => 'Param tidak lengkap'
            ]);
        }

        $host = env('DB_HOST');
        $dbname = env('DB_DATABASE');
        $conn = new PDO("mysql:=$host;dbname=$dbname", env('DB_USERNAME'), env('DB_PASSWORD'));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $exec = $conn->query('SELECT * FROM users where id = 4')->fetchAll(PDO::FETCH_OBJ);
    }
}
