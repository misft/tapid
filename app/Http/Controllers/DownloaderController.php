<?php

namespace App\Http\Controllers;

use crocodicstudio\crudbooster\helpers\CRUDBooster;
use DB;

class DownloaderController extends Controller
{
    public function visitorRecently()
    {
        if (empty(CRUDBooster::myId())) {
            return redirect('/admin/login');
        }

        if (CRUDBooster::isSuperadmin()) {
            $data = DB::table('history')
                ->leftJoin('cms_users', 'history.merchants_id', 'cms_users.id')
                ->leftJoin('users', 'history.users_id', 'users.id')
                ->select('users.name as name', 'cms_users.name as merchant_name', 'history.check_in', 'history.check_out')
                ->orderBy('history.check_in', 'desc')
                ->get();
        } else {
            $data = DB::table('history')
                ->leftJoin('cms_users', 'history.merchants_id', 'cms_users.id')
                ->leftJoin('users', 'history.users_id', 'users.id')
                ->select('users.name as name', 'cms_users.name as merchant_name', 'history.check_in', 'history.check_out')
                ->where('history.merchants_id', CRUDBooster::myId())
                ->orderBy('history.check_in', 'desc')
                ->get();
        }
        $data = collect($data)->map(function ($x) {
            return (array)$x;
        })->toArray();
        $header = [
            'Nama' => 'string',
            'Nama Merchant' => 'string',
            'Check In' => 'string',
            'Check Out' => 'string'
        ];

        $writer = new XLSXWriter();
        $writer->writeSheetHeader('Sheet1', $header);
        foreach ($data as $row)
            $writer->writeSheetRow('Sheet1', $row);
        $filename = 'export_' . date('Ymdhis') . '_visitor_recently.xlsx';
        $writer->writeToFile($filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function users()
    {
        if (empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }
        $data = DB::table('history')
            ->leftJoin('cms_users', 'history.merchants_id', 'cms_users.id')
            ->leftJoin('users', 'history.users_id', 'users.id')
            ->select('users.name as name', 'cms_users.name as merchant_name', 'history.check_in', 'history.check_out')
            ->orderBy('history.check_in', 'desc')
            ->get();
        $header = [
            'Nama' => 'string',
            'Nama Merchant' => 'string',
            'Check In' => 'string',
            'Check Out' => 'string'
        ];
        $filename = 'export_' . date('Ymdhis') . '_visitor_recently.xlsx';

        $this->exportExcel($data, $header, $filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function clusters()
    {
        if (empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }

        $data = DB::table('cluster_otg')
            ->leftJoin('history', 'history.id', 'cluster_otg.history_id')
            ->leftJoin('cms_users', 'cms_users.id', 'cluster_otg.merchants_id')
            ->leftJoin('users', 'users.nik', 'cluster_otg.users_nik')
            ->groupBy('cluster_otg.notifications_id')
            ->select('cms_users.name as merchant_name')
            ->selectRaw('(select max(check_in) from history as h where h.users_id = users.id and h.merchants_id = cms_users.id) as start_at')
            ->selectRaw('(select max(check_out) from history as h where h.users_id = users.id and h.merchants_id = cms_users.id) as end_at')
            ->selectRaw('(select count(*) from otg where otg.notifications_id = cluster_otg.notifications_id and otg.start_at > "' . date('Y-m-d H:i:s', strtotime('-14 days', date('Y-m-d H:i:s'))) . '") as otg_count')
            ->selectRaw('(select count(*) from otg where otg.notifications_id = cluster_otg.notifications_id and otg.start_at < "' . date('Y-m-d H:i:s', strtotime('-14 days', date('Y-m-d H:i:s'))) . '") as quarantine_count')
            ->selectRaw('(select timediff(current_date, max(start_at)) from otg where otg.notifications_id = cluster_otg.notifications_id and otg.start_at > "' . date('Y-m-d H:i:s', strtotime('-14 days', date('Y-m-d H:i:s'))) . '") as quarantine_days_left')
            ->get();

        $header = [
            'Nama' => 'string',
            'Nama Merchant' => 'string',
            'Mulai dari' => 'string',
            'Sampai' => 'string',
            'Jumlah OTG' => 'integer',
            'Jumlah Karantina' => 'integer',
            'Sisa Karantina' => 'integer',
        ];
        $filename = 'export_' . date('Ymdhis') . '_clusters.xlsx';
        $this->exportExcel($data, $header, $filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function otg()
    {
        if (empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }

        $data = DB::table('otg')
            ->leftJoin('users', 'users.id', 'otg.users_id')
            ->where('otg.start_at', '>=', date("Y-m-d H:i:s", strtotime("-14 days")))
            ->select('cms_users.name', 'otg.start_at')
            ->selectRaw('DATE_ADD(otg.start_at, INTERVAL 14 DAY) as end_at')
            ->orderBy('otg.start_at', 'desc')
            ->get();
        $header = [
            'Nama' => 'string',
            'Mulai Karantina' => 'string',
            'Selesai Karantina' => 'string'
        ];
        $filename = 'export_' . date('Ymdhis') . '_otg.xlsx';
        $this->exportExcel($data, $header, $filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function quarantine()
    {
        if (empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }

        $data = DB::table('otg')
            ->leftJoin('users', 'users.id', 'otg.users_id')
            ->where('otg.start_at', '<=', date("Y-m-d H:i:s", strtotime("-14 days")))
            ->select('users.name', 'otg.start_at')
            ->selectRaw('DATE_ADD(otg.start_at, INTERVAL 14 DAY) as end_at')
            ->orderBy('otg.start_at', 'desc')
            ->get();
        $header = [
            'Nama' => 'string',
            'Mulai Karantina' => 'string',
            'Selesai Karantina' => 'string'
        ];
        $filename = 'export_' . date('Ymdhis') . '_quarantine.xlsx';
        $this->exportExcel($data, $header, $filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function overloadMerchants()
    {
        if (empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }
        $data = DB::table('cms_users')
            ->leftJoin('bussiness_type', 'bussiness_type.id', 'cms_users.bussiness_type_id')
            ->whereRaw('round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) >= 100')
            ->select('cms_users.name', 'cms_users.visitor_count')
            ->selectRaw('floor(capacity * bussiness_type.capacity_percentage) as capacity_pandemic')
            ->get();
        $header = [
            'Nama Merchant' => 'string',
            'Jumalh Pengunjung' => 'string',
            'Kapasitas PSBB' => 'integer',
        ];
        $filename = 'export_' . date('Ymdhis') . '_overload_merchants.xlsx';

        $this->exportExcel($data, $header, $filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function merchantCondition()
    {
        if (empty(CRUDBooster::myId())) {
            return redirect('/admin/login');
        }

        if (!CRUDBooster::isSuperadmin()) {
            return back();
        }
        $data = DB::table('cms_users')
            ->leftJoin('bussiness_type', 'bussiness_type.id', 'cms_users.bussiness_type_id')
            ->select('cms_users.name')
            ->selectRaw("round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) as percentage")
            ->selectRaw("if(round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) < 80, 'Safe', if(round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) >= 100, 'Danger', 'Warning')) as zone")
            ->whereRaw('round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) > 0')
            ->get();
        $header = [
            'Nama Merchant' => 'string',
            'Persentase' => 'string',
            'Status' => 'string'
        ];
        $filename = 'export_' . date('Ymdhis') . '_merchant_condition.xlsx';
        $this->exportExcel($data, $header, $filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    public function overcapacityHistory()
    {
        if (empty(CRUDBooster::myId())) {
            return redirect('/admin/login');
        }

        if (CRUDBooster::isSuperadmin()) {
            $data = DB::table('overcapacity_history')
                ->leftJoin('cms_users', 'overcapacity_history.merchants_id', 'cms_users.id')
                ->groupBy('overcapacity_history.merchants_id')
                ->selectRaw('cms_users.name, count(*) as sum, date_format(sec_to_time(avg(time_to_sec(timediff(overcapacity_history.end_at, overcapacity_history.start_at)))), "%h jam %m menit %s detik") as average')
                ->get();
            $header = [
                'Nama Merchant' => 'string',
                'Point' => 'integer',
                'Rata - rata' => 'string'
            ];
            $filename = 'export_' . date('Ymdhis') . '_overcapacity_history.xlsx';
            $this->exportExcel($data, $header, $filename);

            return response()->download($filename)->deleteFileAfterSend(true);
        }
        $data = DB::table('overcapacity_history')
            ->leftJoin('cms_users', 'overcapacity_history.merchants_id', 'cms_users.id')
            ->where('overcapacity_history.merchants_id', CRUDBooster::myId())
            ->select()
            ->get();

        $header = [
            'Tanggal' => 'string',
            'Jam' => 'string',
            'Durasi' => 'string'
        ];
        $filename = 'export_' . date('Ymdhis') . '_overcapacity_history.xlsx';
        $this->exportExcel($data, $header, $filename);

        return response()->download($filename)->deleteFileAfterSend(true);
    }

    function exportExcel($data, $header, $filename)
    {
        $data = collect($data)->map(function ($x) {
            return (array)$x;
        })->toArray();

        $writer = new XLSXWriter();
        $writer->writeSheetHeader('Sheet1', $header);
        foreach ($data as $row)
            $writer->writeSheetRow('Sheet1', $row);
        $writer->writeToFile($filename);
    }
}
