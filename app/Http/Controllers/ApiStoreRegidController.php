<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiStoreRegidController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "cms_users";
				$this->permalink   = "store_regid";
				$this->method_type = "post";
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $postdata = CRUDBooster::valid([
                    'id'=>'required',
                    'regid'=>'required'
                ]);

                DB::table('cms_users')->where('id', $postdata['id'])->update([
                    'regid'=>$postdata['regid']
                ]);
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
