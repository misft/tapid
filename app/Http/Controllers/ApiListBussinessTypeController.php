<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiListBussinessTypeController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "bussines_type";
				$this->permalink   = "list_bussiness_type";
				$this->method_type = "get";
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $postdata['api_status'] = 1;
                $postdata['api_message'] = 'success';
                $postdata['data'] = DB::table('bussiness_type')->get();

                return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
