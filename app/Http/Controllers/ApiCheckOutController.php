<?php namespace App\Http\Controllers;

        use Session;
		use Request;
		use DB;
		use CRUDBooster;
		use Log;

		//Model
        use App\History;
        use App\Users;
        use App\Merchants;

		class ApiCheckOutController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $users_repo;
		    protected $merchants_repo;
		    protected $history_repo;

		    function __construct() {
				$this->table       = "history";
				$this->permalink   = "check_out";
				$this->method_type = "post";

                //Init repo
                $this->users_repo = new \App\Repositories\UsersRepository(new Users());
                $this->merchants_repo = new \App\Repositories\MerchantsRepository(new Merchants());
                $this->history_repo = new \App\Repositories\HistoryRepository(new History());
		    }


		    public function hook_before(&$postdata) {
				//Get the secret checkin
				$postdata = CRUDBooster::valid([
					'id'=>'required'
				], [
					'required'=>'Oops, ada yang salah'
				]);

				//Get record
				$id = $this->history_repo->find($postdata['id']);

				//If record doesnt exist, send response
				if($id == null) {
					return response()->json([
						'api_status'=>0,
						'api_message'=>'Maaf, data check in tidak ditemukan :('
					])->send();
				}

				//Send notification to third party
                $this->sendNotificationToThirdParty($this->merchants_repo->find($id->merchants_id));

                //Update the history
                $this->history_repo->updateAfterCheckout($postdata['id'], $id->check_in);

				//Remove user checkin flag
                $this->users_repo->updateAfterCheckOut($id->users_id);

				//Retrieve the latest update
				$id = $this->history_repo->find($postdata['id']);

				//Send Notification
				try {
					$regid = $this->users_repo->getRegidById($id->users_id);
					$payload = [
						'title'=>'Berhasil Check-Out',
						'content'=>$this->merchants_repo->findMerchantNameById($id->merchants_id),
						'date'=>$id->check_out,
                        'type'=>'check_out',
						'id_check_out'=>$id->id
					];
					$postdata['fcm'] = CRUDBooster::sendFCM($regid, $payload);
				} catch (\Throwable $th) {
					throw $th;
				}

                //Update visitor count
                $this->merchants_repo->updateWhereId($id->merchants_id, [
                    'visitor_count'=>$this->history_repo->countCheckInMerchantById($id->merchants_id)
                ]);

                //Prepare the response
				$postdata['api_status'] = 1;
				$postdata['api_message'] = 'success';
				$postdata['data'] = $id;
				$postdata['data']->is_checkin = 0;
				$postdata['data']->merchant_name = $this->merchants_repo->findMerchantNameById($id->merchants_id);

				//Send response
				return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
				exit;
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		    private function sendNotificationToThirdParty($merchant) {
                $curl = curl_init("https://d3vctrace.c-trace.co.id/api/tap/checkinout");
                $fields = [
                    "act"=>"send_checkinout",
                    "tenant_kode_jsc"=>"$merchant->jsc_code",
                    "type_inout"=>1,
                    "date_tap"=>date('Y-m-d')
                ];
                $token = env('C-TRACE_API_KEY');
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("api_token: bearer $token"));
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($curl);
                Log::info([$fields, $response]);
                $response = json_decode($response);
            }

		}
