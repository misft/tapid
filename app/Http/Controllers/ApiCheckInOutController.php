<?php

namespace App\Http\Controllers;

use CRUDBooster;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use PDO;

class ApiCheckInOutController extends Controller
{
    public function checkIn(Request $req)
    {
        $postdata = $req->all();
        if (empty($postdata['id_user']) || empty($postdata['secret'])) {
            return response([
                'api_status' => 0,
                'api_message' => 'Param tidak lengkap'
            ]);
        }

        try {
            $secret = decrypt($postdata['secret']);
        } catch (DecryptException $th) {
            return response([
                'api_status' => 0,
                'api_message' => 'Kode QR tidak valid'
            ]);
        }

        $conn = $this->createConnection();

        $exec = $conn->prepare("SELECT id from history WHERE users_id = ? AND check_out IS NULL LIMIT 1");
        $exec->bindParam(1, $postdata['id_user'], PDO::PARAM_INT);
        $exec->execute();
        $result = $exec->fetchObject()->id;
        if (!empty($result)) {
            return response([
                'api_status' => 0,
                'api_message' => 'Anda belum check out dari tempat lain'
            ]);
        }

        $exec = $conn->prepare("SELECT * from cms_users where id = ? LIMIT 1");
        $exec->bindParam(1, $secret['id'], PDO::PARAM_INT);
        $exec->execute();
        $merchant = $exec->fetchObject();

        $this->sendNotificationToThirdParty($merchant);

        $now = date('Y-m-d H:i:s');
        $exec = $conn->prepare("INSERT INTO history (users_id, merchants_id, check_in) VALUES (?, ?, ?)");
        $exec->bindParam(1, $postdata['id_user'], PDO::PARAM_INT);
        $exec->bindParam(2, $secret['id'], PDO::PARAM_INT);
        $exec->bindParam(3, $now, PDO::PARAM_STR);
        $exec->execute();
        $history_id = $conn->lastInsertId();

        $exec = $conn->prepare("UPDATE users SET is_checkin = 1 where id = ?");
        $exec->bindParam(1, $postdata['id_user'], PDO::PARAM_INT);
        $exec->execute();

        $exec = $conn->prepare("SELECT * from history where id = ? LIMIT 1");
        $exec->bindParam(1, $history_id, PDO::PARAM_INT);
        $exec->execute();
        $history = $exec->fetchObject();

        $exec = $conn->prepare("SELECT regid from users where id = ? LIMIT 1");
        $exec->bindParam(1, $postdata['id_user'], PDO::PARAM_INT);
        $exec->execute();
        $regid = $exec->fetchObject()->regid;

        $payload = [
            'title' => 'Berhasil Check In',
            'content' => $secret['name'],
            'date' => $history->check_in,
            'type' => 'check_in',
            'id_check_in' => $history_id
        ];
        $postdata['fcm'] = CRUDBooster::sendFCM([$regid], $payload);

        $exec = $conn->prepare("SELECT count(*) as visitor_count from history where merchants_id = ? LIMIT 1");
        $exec->bindParam(1, $secret['id'], PDO::PARAM_INT);
        $exec->execute();
        $visitor_count = $exec->fetchObject()->visitor_count;

        $exec = $conn->prepare("UPDATE cms_users SET visitor_count = ? where id = ?");
        $exec->bindParam(1, $visitor_count, PDO::PARAM_INT);
        $exec->bindParam(2, $secret['id'], PDO::PARAM_INT);
        $exec->execute();

        //Send notif to merchant if its full
        $exec = $conn->prepare("SELECT * from cms_users where id = ? LIMIT 1");
        $exec->bindParam(1, $secret['id'], PDO::PARAM_INT);
        $exec->execute();
        $merchant = $exec->fetchObject();

        $exec = $conn->prepare("SELECT * from bussiness_type where id = ? LIMIT 1");
        $exec->bindParam(1, $merchant->bussiness_type_id, PDO::PARAM_INT);
        $exec->execute();
        $capacity_percentage = $exec->fetchObject()->capacity_percentage;

        if ($merchant->visitor_count >= ($merchant->capacity * $capacity_percentage)) {
            $payload = [
                'title' => 'Kapasitas penuh',
                'content' => 'Seseorang baru saja check in sedangkan kapasitas merchant anda penuh',
            ];
            $postdata['fcm_merchant'] = CRUDBooster::sendFCM([$merchant->regid], $payload);

            //Insert to overcapacity history
        }

        //Prepare the response
        $postdata['api_status'] = 1;
        $postdata['api_message'] = 'success';
        $postdata['data'] = $history;
        $postdata['data']->is_checkin = 1;
        $postdata['data']->merchant_name = $merchant->name;

        //Return response
        return response($postdata);
    }

    public function checkOut(Request $req)
    {
        $postdata = $req->all();
        if (empty($postdata['id'])) {
            return response([
                'api_status' => 0,
                'api_message' => 'Param tidak lengkap'
            ]);
        }

        $conn = $this->createConnection();

        $exec = $conn->prepare("SELECT id from history WHERE id = ? LIMIT 1");
        $exec->bindParam(1, $postdata['id'], PDO::PARAM_INT);
        $exec->execute();
        $history_id = $exec->fetchObject()->id;
        if (empty($history_id)) {
            return response()->json([
                'api_status' => 0,
                'api_message' => 'Maaf, data check in tidak ditemukan :('
            ]);
        }

        $exec = $conn->prepare("SELECT * from history where id = ? LIMIT 1");
        $exec->bindParam(1, $history_id, PDO::PARAM_INT);
        $exec->execute();
        $history = $exec->fetchObject();

        $exec = $conn->prepare("SELECT * from cms_users where id = ? LIMIT 1");
        $exec->bindParam(1, $history->merchants_id, PDO::PARAM_INT);
        $exec->execute();
        $merchant = $exec->fetchObject();

        $this->sendNotificationToThirdParty($merchant);

        $date = date('Y-m-d H:i:s');

        //Update the history
        $exec = $conn->prepare("UPDATE history SET check_out = ? where id = ?");
        $exec->bindParam(1, $date, PDO::PARAM_STR);
        $exec->bindParam(2, $history_id, PDO::PARAM_INT);
        $exec->execute();

        //Remove user checkin flag
        $exec = $conn->prepare("UPDATE users SET is_checkin = 0 where id = ?");
        $exec->bindParam(1, $history->users_id, PDO::PARAM_INT);
        $exec->execute();

        $exec = $conn->prepare("SELECT * from history where id = ? LIMIT 1");
        $exec->bindParam(1, $history_id, PDO::PARAM_INT);
        $exec->execute();
        $history = $exec->fetchObject();

        try {
            $exec = $conn->prepare("SELECT regid from users where id = ? LIMIT 1");
            $exec->bindParam(1, $history->users_id, PDO::PARAM_INT);
            $exec->execute();
            $regid = $exec->fetchObject()->regid;

            $payload = [
                'title' => 'Berhasil Check-Out',
                'content' => $merchant->name,
                'date' => $history->check_out,
                'type' => 'check_out',
                'id_check_out' => $history->id
            ];
            $postdata['fcm'] = CRUDBooster::sendFCM([$regid], $payload);
        } catch (\Throwable $th) {
            throw $th;
        }

        $exec = $conn->prepare("SELECT count(*) as visitor_count from history where merchants_id = ? LIMIT 1");
        $exec->bindParam(1, $history->merchants_id, PDO::PARAM_INT);
        $exec->execute();
        $visitor_count = $exec->fetchObject()->visitor_count;

        $exec = $conn->prepare("SELECT * from bussiness_type where id = ? LIMIT 1");
        $exec->bindParam(1, $merchant->bussiness_type_id, PDO::PARAM_INT);
        $exec->execute();
        $capacity_percentage = $exec->fetchObject()->capacity_percentage;

        $exec = $conn->prepare("UPDATE cms_users SET visitor_count = ? where id = ?");
        $exec->bindParam(1, $visitor_count, PDO::PARAM_INT);
        $exec->bindParam(2, $history->merchants_id, PDO::PARAM_INT);
        $exec->execute();

        $exec = $conn->prepare("SELECT id from overcapacity_history WHERE merchants_id = ? AND end_at IS NULL LIMIT 1");
        $exec->bindParam(1, $history->merchants_id, PDO::PARAM_INT);
        $exec->execute();
        $overcapacity_history_id = $exec->fetchObject()->id;

        //End overcapacity history if record exist
        if($visitor_count * ($capacity_percentage / 100) < $capacity_percentage && !empty($overcapacity_history_id)) {
            $exec = $conn->prepare("UPDATE overcapacity_history SET end_at = ? where id = ?");
            $exec->bindParam(1, $date, PDO::PARAM_STR);
            $exec->bindParam(2, $overcapacity_history_id, PDO::PARAM_INT);
            $exec->execute();
        }

        //Prepare the response
        $postdata['api_status'] = 1;
        $postdata['api_message'] = 'success';
        $postdata['data'] = $history;
        $postdata['data']->is_checkin = 0;
        $postdata['data']->merchant_name = $merchant->name;

        //Send response
        return response($postdata);
    }

    private function createConnection()
    {
        $host = env('DB_HOST');
        $dbname = env('DB_DATABASE');

        $conn = new PDO("mysql:=$host;dbname=$dbname", env('DB_USERNAME'), env('DB_PASSWORD'));
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $conn;
    }

    private function sendNotificationToThirdParty($merchant)
    {
        $curl = curl_init("https://d3vctrace.c-trace.co.id/api/tap/checkinout");
        $fields = [
            "act" => "send_checkinout",
            "tenant_kode_jsc" => "$merchant->jsc_code",
            "type_inout" => 0,
            "date_tap" => date('Y-m-d')
        ];
        $token = env('C-TRACE_API_KEY');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("api_token: bearer $token"));
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $response = json_decode($response);
    }
}
