<?php namespace App\Http\Controllers;

		use Illuminate\Support\Facades\Log;
        use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiCheckInMerchantController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "history";
				$this->permalink   = "check_in_merchant";
				$this->method_type = "post";
		    }


		    public function hook_before(&$postdata) {
				//Validate required data
				$postdata = CRUDBooster::valid([
					'id_merchant'=>'required|integer',
					'secret'=>'required|string'
				]);

				//Decode request params
				try {
					$secret = decrypt($postdata['secret']);
				} catch (\Throwable $th) {
					return response([
						'api_status'=>0,
						'api_message'=>'Kode QR tidak valid'
					])->send();
				}

                //Get user data
                $user = DB::table('users')->where('nik', $secret['nik'])->first();

                //Check if user is checking in in other merchant
                $is_checkin = DB::table('history')
                    ->where('users_id', $user->id)
                    ->whereNull('check_out')
                    ->exists();
                if($is_checkin) {
                    $regid = DB::table('users')->where('id', $user->id)->pluck('regid');
                    $postdata['fcm'] = CRUDBooster::sendFCM($regid, [
                        'title'=>'Anda belum checkout',
                        'content'=>"Mohon maaf anda belum dapat check in karena anda belum checkout dari tempat lain",
                        'id_check_in'=>DB::table('history')
                            ->where('users_id', $user->id)
                            ->whereNull('check_out')
                            ->first()->id
                    ]);
                    return response()->json([
                        'api_status'=>0,
                        'api_message'=>'User terdeteksi belum checkout dari tempat lain'
                    ])->send();
                }

				//Insert to history
				$id = DB::table('history')->insertGetId([
					'users_id'=>$user->id,
					'merchants_id'=>$postdata['id_merchant'],
					'check_in'=>date('Y-m-d H:i:s'),
				]);

				//Add user checkin flag
				DB::table('users')->where('nik', $secret['nik'])->update([
					'is_checkin'=>1
				]);

				//Get history data
				$history = DB::table('history')->where('id', $id)->first();

				//Send notification
				try {
					$regid = DB::table('users')->where('id', $user->id)->pluck('regid');
					$postdata['fcm'] = CRUDBooster::sendFCM($regid, [
						'title'=>'Berhasil Check In',
						'content'=>DB::table('merchants')->where('id', $postdata['id_merchant'])->first()->name,
						'date'=>$history->in,
						'id_check_in'=>$id
					]);
				} catch (\Throwable $th) {
				}

				//Increase visitor count
                DB::table('cms_users')->where('id', $postdata["id_merchant"])->update([
                    'visitor_count'=>DB::table('history')->where('merchants_id', $postdata['id_merchant'])->whereNull('check_out')->count()
                ]);

                //Check if merchant is overload and send notif
                $merchant = DB::table('cms_users')->where('id', $postdata["id_merchant"])->first();
                try {
                    $capacity_percentage = DB::table('bussiness_type')->where('id', $merchant->bussiness_type_id)->first()->capacity_percentage;
                    $max_capacity = $merchant->capacity * $capacity_percentage;
                    $percentage = ($merchant->visitor_count / $max_capacity) * 100;
                    if ($percentage > 100) {
                        $payload = [
                            'title' => 'Kapasitas Penuh!!',
                            'content' => 'Sesorang baru saja check in, sedangkan kapasitas penuh'
                        ];
                        CRUDBooster::sendFCM([$merchant->regid], $payload);
                    }
                }
                catch(\Exception $e) {

                }


                //Prepare response
				$postdata['api_status'] = 1;
				$postdata['api_message'] = 'success';
				$postdata['data'] = DB::table('history')->where('id', $id)->first();
				$postdata['data']->is_checkin = 1;

				//Send response
				return response($postdata, 200)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
				exit;
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
