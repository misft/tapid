<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiLogoutController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {    
				$this->table       = "users";        
				$this->permalink   = "logout";    
				$this->method_type = "post";    
		    }
		

		    public function hook_before(&$postdata) {
				//Validate required data
				$postdata = CRUDBooster::valid([
					'id_user'=>'required'
				]);

				//Update the user check in flag
				DB::table('users')->where('id', $postdata['id_user'])->update([
					'is_checkin'=>0
				]);

				//Return response success
				return response()->json([
					'api_status'=>1,
					'api_message'=>'success'
				])->send();
				exit;
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
				exit;
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				exit;
		    }

		}