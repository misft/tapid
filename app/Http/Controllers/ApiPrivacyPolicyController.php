<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiPrivacyPolicyController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "privacy_policy";
				$this->permalink   = "privacy_policy";
				$this->method_type = "get";
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $postdata['api_status'] = 1;
                $postdata['api_message'] = 'success';
                $postdata['data'] = DB::table('privacy_policy')->orderBy('created_at', 'desc')->first();

                return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
