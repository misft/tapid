<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		//Model
        use App\History;

		class ApiContactedTracingHistoryController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $history_repo;

            function __construct() {
				$this->table       = "history";
				$this->permalink   = "contacted_tracing_history";
				$this->method_type = "post";

				//Init repo
                $this->history_repo = new \App\Repositories\HistoryRepository(new History());
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $postdata = CRUDBooster::valid([
                    'signature'=>'required',
                    'tstamp'=>'required',
                    'date_start'=>'required'
                ], [
                    'required'=>'Param field belum lengkap'
                ]);

                $history = $this->history_repo->getContactedTracingHistory($postdata['date_start']);

                return response()->json($history)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
