<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		//Model
        use App\History;
        use App\Users;

		class ApiClusteredHistoryController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $users_repo;
		    protected $history_repo;

		    function __construct() {
				$this->table       = "history";
				$this->permalink   = "clustered_history";
				$this->method_type = "post";

				//Init repo
				$this->history_repo = new \App\Repositories\HistoryRepository(new History());
				$this->users_repo = new \App\Repositories\UsersRepository(new Users());
		    }


		    public function hook_before(&$postdata) {
		        //Validate data
                $postdata = CRUDBooster::valid([
                    'signature'=>'required',
                    'tstamp'=>'required',
                    'date_start'=>'required',
                    'date_end'=>'required'
                ], [
                    'required'=>'Param belum lengkap'
                ]);

                $history = $this->history_repo->getClusteredHistory($postdata['date_start'], $postdata['date_end']);

                return response()->json($history)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
