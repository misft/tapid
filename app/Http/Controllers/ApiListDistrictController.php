<?php namespace App\Http\Controllers;

use CRUDBooster;
use DB;
use Request;
use Session;

class ApiListDistrictController extends \crocodicstudio\crudbooster\controllers\ApiController
{

    function __construct()
    {
        $this->table = "district";
        $this->permalink = "list_district";
        $this->method_type = "get";
    }


    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
        $postdata['code'] = g('code');
        $postdata['name'] = g('name');

        $code = $postdata['code'];
        $name = $postdata['name'];

        $postdata['api_status'] = 1;
        $postdata['api_message'] = 'success';

        if (!empty($code)) {
            $postdata['data'] = DB::table('district')
                ->whereRaw("`code` REGEXP '^[0-9]{2}[,.][0-9]{2}[,.][0-9]{2}$'")
                ->where('code', 'like', "$code%")
                ->select('code', 'name')
                ->get();
        } else if(!empty($name)){
            $postdata['data'] = DB::table('district')
                ->where('name', 'like', "%$name%")
                ->select('code', 'name')
                ->get();
        }
        else {
            $postdata['data'] = DB::table('district')
                ->select('code', 'name')
                ->get();
        }

        return response()->json($postdata)->send();
    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query

    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process

    }

}
