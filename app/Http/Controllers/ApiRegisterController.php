<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;
		use Validator;
		use Illuminate\Support\Facades\Hash;

        //Model
        use App\Users;

		class ApiRegisterController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $users_repo;

		    function __construct() {
				$this->table       = "users";
				$this->permalink   = "register";
				$this->method_type = "post";

                //Init repo
                $this->users_repo = new \App\Repositories\UsersRepository(new Users());
		    }


		    public function hook_before(&$postdata) {
                //This method will be execute before run the main process
                $postdata = CRUDBooster::valid([
                    'nik'=>'required|min:16|max:16',
                    'selfie'=>'required|file|mimes:jpg,jpeg,bmp,png,webp',
                    'ktp'=>'required|file|mimes:jpg,jpeg,bmp,png,webp',
                    'email'=>'required|email:rfc,dns',
                    'name'=>'required',
                    'password'=>'required|min:6|max:16',
                    'address'=>'required',
                    'birth'=>'required',
                    'city'=>'required',
                    'district'=>'required',
                    'gender'=>'required',
                    'religion'=>'required',
                    'phone'=>'required',
                    'regid'=>'required'
                ], [
                    'required'=>':Attribute harus diisi',
                    'min'=>':Attribute kurang panjang',
                    'max'=>':Attribute terlalu panjang',
                    'mimes'=>'Ekstensi foto selfie tidak valid',
                    'email'=>'Email tidak valid'
                ]);

                $postdata['password'] = CRUDBooster::validatePassword($postdata['password']);

                $qr = CRUDBooster::generateQR([
                    'nik'=>$postdata['nik'],
                    'phone'=>$postdata['phone']
                ]);

                $data = [
                    'nik'=>$postdata['nik'],
                    'email'=>$postdata['email'],
                    'name'=>$postdata['name'],
                    'address'=>$postdata['address'],
                    'password'=>Hash::make($postdata['password']),
                    'selfie'=>CRUDBooster::uploadFile('selfie', true),
                    'ktp'=>CRUDBooster::uploadFile('ktp', true),
                    'birth'=>$postdata['birth'],
                    'gender'=>$postdata['gender'],
                    'religion'=>$postdata['religion'],
                    'phone'=>$postdata['phone'],
                    'last_login'=>date('Y-m-d'),
                    'regid'=>$postdata['regid'],
                    'city'=>$postdata['city'],
                    'district'=>$postdata['district'],
                    'qrcode'=>CRUDBooster::uploadBase64(base64_encode($qr)),
                    'is_verified'=>'PENDING'
                ];

                $exist = $this->users_repo->isAnyDuplicateExist($postdata['phone'], $postdata['nik'], $postdata['email']);
                if($exist) {
                    return response()->json([
                        'api_status'=>0,
                        'api_message'=>'Nomer telepon / E-mail / NIK sudah digunakan!'
                    ])->send();
                }

                $id = $this->users_repo->insertGetId($data);

                $result['api_status'] = 1;
                $result['api_message'] = 'success';
                $result['data'] = $this->users_repo->find($id);
                $result['data']->id_checkin = 0;
                $result['data']->merchant_name = null;
                unset($result['data']->password);

                return response()->json($result)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
