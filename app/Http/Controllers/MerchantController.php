<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MerchantController extends Controller
{
    public function visitorCheckIn() {
        $data['title'] = "Visitor Check In";
        return view('merchant.check_in', $data);
    }
}
