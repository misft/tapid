<?php namespace App\Http\Controllers;


use App\Users;
use CRUDBooster;
use DB;
use Mail;
use Request;
use Session;

//Model

class ApiForgetPasswordController extends \crocodicstudio\crudbooster\controllers\ApiController
{

    protected $users_repo;

    function __construct()
    {
        $this->table = "users";
        $this->permalink = "forget_password";
        $this->method_type = "post";

        //Init repo
        $this->users_repo = new \App\Repositories\UsersRepository(new Users());
    }


    public function hook_before(&$postdata)
    {
        //This method will be execute before run the main process
        $postdata = CRUDBooster::valid([
            'email' => 'required|email:rfc,dns|max:40'
        ]);

        $email = $postdata['email'];

        $user = $this->users_repo->findWithEmail($email);

        if (empty($user)) {
            return response()->json([
                'api_status' => 0,
                'api_message' => 'Akun dengan e-mail tersebut tidak ditemukan'
            ])->send();
        }
        //Add validation if email exist and validated
        if (!$user->is_verified) {
            return response()->json([
                'api_status' => 0,
                'api_message' => 'Akun dengan e-mail tersebut belum diverifikasi'
            ])->send();
        }

        $email_encrypted = encrypt($postdata['email']);

        //Set expiry
        DB::table('forget_password')->insert([
            'email' => $email,
            'encrypted' => $email_encrypted,
            'expired_at' => date('Y-m-d H:i:s', strtotime("+1 hour"))
        ]);

        //Send email
        \Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
        \Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
        \Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
        \Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
        \Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));

        $data['title'] = "Forget Password";
        $data['user'] = $user;

        $data['link'] = url('/api/apps?secret=' . $email_encrypted);
        $data["image"] = asset('images/img_email_verified.png');

        Mail::send("mail.forget_password", $data, function ($message) use ($data, $email) {
            $message->priority(1);
            $message->to($email);
            $message->from('no-reply@crocodic.net', 'Crocodic');
            $message->subject("Password reset");
        });

        $postdata['api_status'] = 1;
        $postdata['api_message'] = "success";

        return response()->json($postdata)->send();

    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query

    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process

    }

}
