<?php namespace App\Http\Controllers;

        use Illuminate\Contracts\Encryption\DecryptException;
        use Session;
		use Request;
		use DB;
		use CRUDBooster;

        use App\History;

        class ApiCheckQrController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $history_repo;

		    function __construct() {
				$this->table       = "cms_users";
				$this->permalink   = "check_qr";
				$this->method_type = "post";

                $this->history_repo = new \App\Repositories\HistoryRepository(new History());
            }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $postdata = CRUDBooster::valid([
                    'id_user'=>'required',
                    'secret'=>'required'
                ]);


                $postdata['api_status'] = 1;
                $postdata['api_message'] = 'success';
                $postdata['data'] = (object) null;

                try {
                    $decrypted = decrypt($postdata['secret']);
                } catch(DecryptException $e) {
                    return response()->json([
                        'api_status'=>0,
                        'api_message'=>'Kode QR tidak valid'
                    ])->send();
                }

                //Check if users is trying to check in
                //and not checking out elsewhere
                $exist = $this->history_repo->isUserCurrentlyCheckIn($postdata['id_user']);
                if($exist) {
                    return response([
                        'api_status'=>0,
                        'api_message'=>'Anda belum checkout dari tempat lain'
                    ])->send();
                }
                $postdata['data']->decrypted = $decrypted;
                $postdata['data']->merchants_id = $decrypted['id'];
                $postdata['data']->merchant_name = $decrypted['name'];
                $postdata['data']->encrypted = $postdata['secret'];

                return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
