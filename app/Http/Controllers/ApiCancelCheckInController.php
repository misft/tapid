<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiCancelCheckInController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "history";
				$this->permalink   = "cancel_check_in";
				$this->method_type = "post";
		    }


		    public function hook_before(&$postdata) {
				//This method will be execute before run the main process
				$postdata = CRUDBooster::valid([
					'id'=>'required'
				], [
					'required'=>'Oops, terdapat kesalahan'
				]);

				//Get history record
				$history = DB::table('history')->where('id', $postdata['id'])->first();

				DB::table('history')->where('id', $postdata['id'])->delete();

				//Delete check in flag
				DB::table('users')->where('id', $history->users_id)->update([
					'is_checkin'=>0
				]);

				return response([
					'api_status'=>1,
					'api_message'=>'success'
				])->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
				exit;
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
				exit;
		    }

		}
