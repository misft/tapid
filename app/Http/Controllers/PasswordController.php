<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PasswordController extends Controller
{
    public function pageResetPassword() {
        return view("auth.reset_password");
    }
}
