<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiListMerchantController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "cms_users";
				$this->permalink   = "list_merchant";
				$this->method_type = "get";
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $merchant_type_of = g('merchant_type_of');

                $postdata['api_status'] = 1;
                $postdata['api_message'] = 'success';

                if(empty($merchant_type_of)) {
                    $postdata['data'] = DB::table('cms_users')->where('id', '>', 1)->get();
                }
                else {
                    $postdata['data'] = DB::table('cms_users')->where('merchant_type', $merchant_type_of)->get();
                }

                return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
