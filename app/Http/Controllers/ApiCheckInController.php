<?php namespace App\Http\Controllers;

        use Illuminate\Contracts\Encryption\DecryptException;
        use Session;
		use Request;
		use DB;
		use CRUDBooster;
		use Log;

		//Model
        use App\History;
        use App\Users;
        use App\Merchants;

		class ApiCheckInController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    protected $users_repo;
		    protected $merchants_repo;
		    protected $history_repo;

		    function __construct() {
				$this->table       = "history";
				$this->permalink   = "check_in";
				$this->method_type = "post";

				//Init repo
				$this->users_repo = new \App\Repositories\UsersRepository(new Users());
				$this->merchants_repo = new \App\Repositories\MerchantsRepository(new Merchants());
				$this->history_repo = new \App\Repositories\HistoryRepository(new History());
		    }


		    public function hook_before(&$postdata) {
				//Validate the data
				$postdata = CRUDBooster::valid([
					'id_user'=>'required',
					'secret'=>'required'
				], [
					'required'=>'Oops, maaf ada yang salah'
				]);


				//Decode request params
				try {
					$secret = decrypt($postdata['secret']);
                } catch (DecryptException $th) {
                    return response([
						'api_status'=>0,
						'api_message'=>'Kode QR tidak valid'
					])->send();
				}

				//Check if users is trying to check in
				//and not checking out elsewhere
				$exist = $this->history_repo->isUserCurrentlyCheckIn($postdata['id_user']);
				if($exist) {
					return response([
						'api_status'=>0,
						'api_message'=>'Anda belum checkout dari tempat lain'
					])->send();
				}

				//Send notification to third party
                $this->sendNotificationToThirdParty($this->merchants_repo->find($secret['id']));

				//Insert data
				$id = $this->history_repo->insertAfterCheckIn($postdata['id_user'], $secret['id']);

				//Add flag that user is check in
				$this->users_repo->updateAfterCheckIn($postdata['id_user']);

				//Get history
				$history = $this->history_repo->find($id);

				//Send notif
				try {
					$regid = $this->users_repo->getRegidOf($postdata['id_user']);

					$payload = [
						'title'=>'Berhasil Check In',
						'content'=>$secret['name'],
						'date'=>$history->check_in,
                        'type'=>'check_in',
						'id_check_in'=>$id
					];
					$postdata['fcm'] = CRUDBooster::sendFCM($regid, $payload);
				} catch (\Throwable $th) {

				}

				//Update visitor count
                $this->merchants_repo->updateWhereId($secret['id'], [
                    'visitor_count'=>$this->history_repo->countCheckInMerchantById($secret['id'])
                ]);

				try {
                    //Send notif to merchant if its full
                    $merchant = $this->merchants_repo->find($secret['id']);
                    $capacity_percentage = DB::table('bussiness_type')->where('id', $merchant->bussiness_type_id)->first()->capacity_percentage;

                    if($merchant->visitor_count >= ($merchant->capacity * $capacity_percentage)) {
                        $payload = [
                            'title'=>'Kapasitas penuh',
                            'content'=>'Seseorang baru saja check in sedangkan kapasitas merchant anda penuh',
                        ];
                        $postdata['fcm_merchant'] = CRUDBooster::sendFCM([$merchant->regid], $payload);
                    }
                }
                catch(\Exception $e) {

                }

				//Prepare the response
				$postdata['api_status'] = 1;
				$postdata['api_message'] = 'success';
				$postdata['data'] = $history;
				$postdata['data']->is_checkin = 1;
				$postdata['data']->merchant_name = $this->merchants_repo->findMerchantNameById($secret['id']);

				//Return response
				return response()->json($postdata)->send();
				exit;
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
		    }

		    public function hook_after($postdata,&$result) {
				//This method will be execute after run the main process
				exit;
			}

			private function sendNotificationToThirdParty($merchant) {
                $curl = curl_init("https://d3vctrace.c-trace.co.id/api/tap/checkinout");
                $fields = [
                    "act"=>"send_checkinout",
                    "tenant_kode_jsc"=>"$merchant->jsc_code",
                    "type_inout"=>0,
                    "date_tap"=>date('Y-m-d')
                ];
                $token = env('C-TRACE_API_KEY');
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("api_token: bearer $token"));
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($curl);
                Log::info([$fields, $response]);
                $response = json_decode($response);
            }
		}
