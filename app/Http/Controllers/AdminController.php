<?php

namespace App\Http\Controllers;

use CRUDBooster;
use DB;
use Mail;

class AdminController extends Controller
{
    public function index()
    {
        if(empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }

        if (CRUDBooster::isSuperadmin()) {
            $data["chart"]["visitor_recently"] = DB::table('history')
                ->leftJoin('cms_users', 'history.merchants_id', 'cms_users.id')
                ->groupBy('history.merchants_id')
                ->select('cms_users.name')
                ->selectRaw('(select round(visitor_count / (capacity * capacity_percentage) * 100, 2) from cms_users as c where c.id = history.merchants_id) as percentage')
                ->addSelect(DB::raw('floor(cms_users.capacity * cms_users.capacity_percentage) as capacity'))
                ->addSelect('cms_users.visitor_count')
                ->whereNull('history.check_out')
                ->orderBy('percentage', 'desc')
                ->get();
            $data["box"]["cluster_count"] = DB::table('cluster_otg')
                ->selectRaw('(select count(*) from cluster_otg as c group by c.notifications_id) as total')
                ->selectRaw('(
                (select count(*) from cluster_otg as c where c.created_at like "%' . date("Y-m-d", strtotime("-1 day")) . '%" group by c.notifications_id)
                -
                (select count(*) from cluster_otg as c where c.created_at like "%' . date("Y-m-d") . '%" group by c.notifications_id)
                ) as plus')
                ->first();
            $data["box"]["otg_count"] = DB::table('otg')
                ->selectRaw('(select count(*) from otg as o) as total')
                ->selectRaw('(
                (select count(*) from otg as o where o.start_at like "%' . date("Y-m-d") . '%")
                -
                (select count(*) from otg as o where o.start_at like "%' . date("Y-m-d", strtotime("-1 day")) . '%")
                ) as plus')
                ->first();
            $data["box"]["quarantine_count"] = DB::table('otg')
                ->selectRaw('(select count(*) from otg as o) as total')
                ->selectRaw('(
                (select count(*) from otg as o where o.start_at like "%' . date("Y-m-d", strtotime("-14 days")) . '%")
                -
                (select count(*) from otg as o where o.start_at like "%' . date("Y-m-d", strtotime("-15 days")) . '%")
                ) as plus')
                ->first();
            $data["pie"]["merchant_capacity"] = DB::table('cms_users')
                ->leftJoin('bussiness_type', 'bussiness_type.id', 'cms_users.bussiness_type_id')
                ->selectRaw("round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) as percentage")
                ->selectRaw("if(round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) < 80, 'Safe', if(round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) >= 100, 'Danger', 'Warning')) as zone")
                ->whereRaw('round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) > 0')
                ->get();
            $data['chart']['clusters'] = DB::table('cluster_otg')
                ->groupBy(DB::raw('DATE_FORMAT(created_at, "%d")'))
                ->selectRaw('count(*) as sum, date_format(created_at, "%Y-%m-%d") as start_at, notifications_id')
                ->get();
            $data['chart']['otg'] = DB::table('otg')
                ->groupBy(DB::raw('DATE_FORMAT(start_at, "%d")'))
                ->selectRaw('count(*) as sum, DATE_FORMAT(start_at, "%Y-%m-%d") as start_at')
                ->get();
            $data['chart']['quarantine'] = DB::table('otg')
                ->selectRaw('count(*) as sum, date_format(start_at, "%Y-%m-%d") as start_at')
                ->whereDate('start_at', '>', date('Y-m-d H:i:s', strtotime("-14 days")))
                ->groupBy(DB::raw('DATE_FORMAT(start_at, "%d")'))
                ->get();
            $data['table']['overload_merchant'] = DB::table('cms_users')
                ->leftJoin('bussiness_type', 'bussiness_type.id', 'cms_users.bussiness_type_id')
                ->whereRaw('round(visitor_count / (capacity * bussiness_type.capacity_percentage) * 100, 2) >= 100')
                ->get();
            $data['table']['overcapacity_history'] = DB::table('overcapacity_history')
                ->leftJoin('cms_users', 'overcapacity_history.merchants_id', 'cms_users.id')
                ->groupBy('overcapacity_history.merchants_id')
                ->selectRaw('cms_users.name, count(*) as sum, date_format(sec_to_time(avg(time_to_sec(timediff(overcapacity_history.end_at, overcapacity_history.start_at)))), "%h jam %m menit %s detik") as average')
                ->get();

            return view('admin.dashboard', $data);
        }
        $id = CRUDBooster::myId();
        $data['chart']['visitor_recently'] = DB::table('history')
            ->selectRaw('DATE_FORMAT(check_in, "%Y-%m-%d %H:%i") as check_in')
            ->selectRaw('count(*) as visitor_count')
            ->where('history.merchants_id', $id)
            ->groupBy(DB::raw('DATE_FORMAT(check_in, "%Y-%m-%d %H:%i")'))
            ->limit(45)
            ->get();
        $data['merchant'] = DB::table('cms_users')->where('id', CRUDBooster::myId())->first();
        $data['capacity'] = DB::table('bussiness_type')->where('id', $data['merchant']->id)->first();
        $data['capacity_max'] = ($data['capacity']->capacity_percentage / 100 ) * $data['merchant']->capacity;
        $data['box']['visitor_count'] = DB::table('history')
            ->leftJoin('users', 'history.users_id', 'users.id')
            ->select('history.check_in', 'history.check_out', 'users.name')
            ->whereNull('history.check_out')
            ->where('history.check_in', 'like', "%" . date('Y-m-d') . "%")
            ->where('history.merchants_id', $id)
            ->count();
        $data['box']['capacity'] = DB::table('cms_users')
            ->leftJoin('bussiness_type', 'bussiness_type.id', 'cms_users.bussiness_type_id')
            ->where('cms_users.id', $id)
            ->select('cms_users.capacity', 'bussiness_type.capacity_percentage')
            ->first();
        $data['table']['overcapacity_history'] = DB::table('overcapacity_history')
            ->leftJoin('cms_users', 'overcapacity_history.merchants_id', 'cms_users.id')
            ->where('overcapacity_history.merchants_id', CRUDBooster::myId())
            ->get();

        return view('merchant.dashboard', $data);
    }

    public function clusters() {
        if(empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }

        $data['table']['clusters'] = DB::table('cluster_otg')
            ->leftJoin('history', 'history.id', 'cluster_otg.history_id')
            ->leftJoin('cms_users', 'cms_users.id', 'cluster_otg.merchants_id')
            ->leftJoin('users', 'users.nik', 'cluster_otg.users_nik')
            ->groupBy('cluster_otg.notifications_id')
            ->select('cms_users.name as merchant_name')
            ->selectRaw('(select max(check_in) from history as h where h.users_id = users.id and h.merchants_id = cms_users.id) as start_at')
            ->selectRaw('(select max(check_out) from history as h where h.users_id = users.id and h.merchants_id = cms_users.id) as end_at')
            ->selectRaw('(select count(*) from otg where otg.notifications_id = cluster_otg.notifications_id and otg.start_at > "' . date('Y-m-d H:i:s', strtotime('-14 days')) . '") as otg_count')
            ->selectRaw('(select count(*) from otg where otg.notifications_id = cluster_otg.notifications_id and otg.start_at < "' . date('Y-m-d H:i:s', strtotime('-14 days')) . '") as quarantine_count')
            ->selectRaw('(select round(time_to_sec(timediff("'.date('Y-m-d H:i:s').'", max(start_at)))/ (3600 * 24)) from otg where otg.notifications_id = cluster_otg.notifications_id and otg.start_at > "' . date('Y-m-d H:i:s', strtotime('-14 days')) . '") as quarantine_days_left')
            ->get();

        return view('admin.clusters', $data);
    }

    public function otg() {
        if(empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }

        $data['table']['otg'] = DB::table('otg')
            ->leftJoin('users', 'users.id', 'otg.users_id')
            ->whereDate('otg.start_at', '>', date('Y-m-d H:i:s', strtotime('-14 days', date('Y-m-d H:i:s'))))
            ->select('users.name', 'otg.start_at')
            ->selectRaw('DATE_ADD(otg.start_at, INTERVAL 14 DAY) as end_at')
            ->selectRaw('DATEDIFF(DATE_ADD(otg.start_at, INTERVAL 14 DAY), "'.date('Y-m-d H:i:s').'") as days_left')
            ->get();

        return view('admin.otg', $data);
    }

    public function quarantine() {
        if(empty(CRUDBooster::isSuperadmin())) {
            return redirect('/admin/login');
        }

        $data['table']['quarantine'] = DB::table('otg')
            ->leftJoin('users', 'users.id', 'otg.users_id')
            ->where('otg.start_at', '<', date('Y-m-d H:i:s', strtotime('-14 days')))
            ->select('users.name')
            ->selectRaw('DATE_ADD(otg.start_at, INTERVAL 14 DAY) as end_at')
            ->get();

        return view('admin.quarantine');
    }

    public function testEmail()
    {
        \Config::set('mail.driver', CRUDBooster::getSetting('smtp_driver'));
        \Config::set('mail.host', CRUDBooster::getSetting('smtp_host'));
        \Config::set('mail.port', CRUDBooster::getSetting('smtp_port'));
        \Config::set('mail.username', CRUDBooster::getSetting('smtp_username'));
        \Config::set('mail.password', CRUDBooster::getSetting('smtp_password'));

        $data["email"] = "indrawandwiprasetyo@gmail.com";
        $data["name"] = "Indra";

        return Mail::send('mail.user_unverified', $data, function ($message) use ($data) {
            $message->from('no-reply@tap.id', 'Tap.id');
            $message->to($data['email']);
            $message->subject('Your account has been banned');
            $message->priority(1);
        });
    }
}
