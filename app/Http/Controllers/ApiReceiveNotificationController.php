<?php namespace App\Http\Controllers;

use App\Notifications;
use App\Users;
use CRUDBooster;
use DB;
use Request;
use Session;

//Model

class ApiReceiveNotificationController extends \crocodicstudio\crudbooster\controllers\ApiController
{

    protected $users_repo;
    protected $notifications_repo;

    function __construct()
    {
        $this->table = "history";
        $this->permalink = "receive_notification";
        $this->method_type = "post";

        //Init repo
        $this->users_repo = new \App\Repositories\UsersRepository(new Users());
        $this->notifications_repo = new \App\Repositories\NotificationsRepository(new Notifications());
    }


    public function hook_before(&$postdata)
    {

    }

    public function hook_query(&$query)
    {
        //This method is to customize the sql query

    }

    public function hook_after($postdata, &$result)
    {
        //This method will be execute after run the main process
        //Validate params
        $postdata = CRUDBooster::valid([
            'signature' => 'required',
            'tstamp' => 'required',
            'notif_msg' => 'required',
            'list_user' => 'required'
        ], [
            'required' => ':Attribute tidak lengkap'
        ]);


        //Parse array
        $postdata['list_user'] = json_encode($postdata['list_user']);

        //Save notification to table notification
        $id = $this->notifications_repo->insertGetId([
            'signature' => $postdata['signature'],
            'created_at' => $postdata['tstamp'],
            'message' => $postdata['notif_msg'],
            'list_user' => $postdata['list_user']
        ]);

        //Set every users to OTG
        $this->users_repo->setOtg($postdata['list_user'], $postdata['tstamp'], $id);

        $list_user = json_decode($postdata['list_user']);

        //Insert to cluster OTG
        $this->createClusterOtg($list_user, $postdata['tstamp'], $id);

        //Get users regid
        $regid = [];
        foreach ($list_user as $x => $user) {
            $regid[$x] = $this->users_repo->getRegidByNIK($user->no_identitas);
        }

        //Send fcm
        try {
            $payload = [
                'title' => 'Notifikasi Umum',
                'content' => $postdata['notif_msg'],
                'date' => $postdata['tstamp'],
                'type' => 'third_party'
            ];
            $postdata['fcm'] = CRUDBooster::sendFCM($regid, $payload);
        } catch (\Throwable $th) {

        }

        //Return the response
        $postdata['api_status'] = 1;
        $postdata['api_message'] = 'success';
        $postdata['data'] = $this->notifications_repo->find($id);

        return response()->json($postdata)->send();
    }

    private function createClusterOtg($list_user, $timestamp, $id_notification)
    {
        foreach ($list_user as $i) {
            //Get the user
            $user_otg = DB::table('users')
                ->where('nik', 'like', "%" . $i->no_identitas . "%")
                ->first();

            //Check where current user check in
            $_cluster = DB::table('history')
                ->where('users_id', $user_otg->id)
                ->WhereDate('check_in', '>=', date('Y-m-d H:i:s', strtotime("-2 days")))
                ->orderBy('id', 'desc')
                ->get();

            foreach ($_cluster as $j) {
                $people_history = DB::table('history')
                    ->where('merchants_id', $j->merchants_id)
                    ->WhereDate('check_in', '>=', date('Y-m-d H:i:s', strtotime("-2 days")))
                    ->orderBy('id', 'desc')
                    ->get();
                foreach ($people_history as $z) {
                    $_user = DB::table('users')->where('id', $z->users_id)->pluck('nik')->first();
                    if (
                        //A person is inside the timeline of suspected
                        ($z->check_in > $j->check_in && $z->check_out < $j->check_out) ||
                        //Suspected is inside the timeline of a person
                        ($z->check_in < $j->check_in && $z->check_out > $j->check_out) ||
                        //A person check in first and leave first
                        ($z->check_in < $j->check_in && $z->check_out < $j->check_out) ||
                        //A person check in second than suspected and leave secoond
                        ($z->check_in > $j->check_in && $z->check_out > $j->check_out)
                    ) {
                        DB::table('cluster_otg')->insert([
                            'merchants_id' => $j->merchants_id,
                            'users_nik' => $_user,
                            'created_at' => $timestamp,
                            'history_id' => $z->id,
                            'notifications_id'=>$id_notification
                        ]);
                    }
                }
            }
        }
    }
}
