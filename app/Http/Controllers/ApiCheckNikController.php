<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiCheckNikController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "users";
				$this->permalink   = "check_nik";
				$this->method_type = "post";
		    }


		    public function hook_before(&$postdata) {
                //Check if NIK valid
                $postdata = CRUDBooster::valid([
                    'nik'=>'required',
                    'name'=>'required',
                    'address'=>'required'
                ], [
                    'required'=>'Oops, ada yang belum anda isi'
                ]);

                $nik = $postdata['nik'];
                $name = $postdata['name'];
                $name = str_replace(" ", "%20", $name);
                $address = $postdata['address'];

                //Get profile
                $curl = curl_init("http://ptversa.com/lacak/public/api/regis?nik=$nik&nama=$name&alamat=$address");
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($curl);
                $response = json_decode($response);

                if($response->api_status == 1)
                    return response()->json($response)->send();

                return response()->json([
                    'api_status'=>0,
                    'api_message'=>'Data tidak ditemukan'
                ])->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query
                exit();
		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process
                exit();
		    }

		}
