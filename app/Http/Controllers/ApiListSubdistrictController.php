<?php namespace App\Http\Controllers;

		use Session;
		use Request;
		use DB;
		use CRUDBooster;

		class ApiListSubdistrictController extends \crocodicstudio\crudbooster\controllers\ApiController {

		    function __construct() {
				$this->table       = "master_place";
				$this->permalink   = "list_subdistrict";
				$this->method_type = "get";
		    }


		    public function hook_before(&$postdata) {
		        //This method will be execute before run the main process
                $postdata = CRUDBooster::valid([
                    'code'=>'required|max:30'
                ]);

                $code = $postdata['code'];

                $postdata['api_status'] = 1;
                $postdata['api_message'] = 'success';
                $postdata['data'] = DB::table('subdistrict')
                    ->whereRaw("`code` REGEXP '^[0-9]{2}[,.][0-9]{2}[,.][0-9]{2}[,.][0-9]{4}$'")
                    ->where('code', 'like', "$code%")
                    ->get();

                return response()->json($postdata)->send();
		    }

		    public function hook_query(&$query) {
		        //This method is to customize the sql query

		    }

		    public function hook_after($postdata,&$result) {
		        //This method will be execute after run the main process

		    }

		}
