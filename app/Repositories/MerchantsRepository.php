<?php namespace App\Repositories;

use App\Merchants;
use App\History;

class MerchantsRepository {
    protected $merchants;
    protected $history;

    public function __construct(Merchants $merchants) {
        $this->merchants = $merchants;
        $this->history = new History();
    }

    /**
     * @param $id
     * @return mixed
     * Return name of a merchant where id is $id
     */
    public function findMerchantNameById($id) {
        return $this->merchants->where('id', $id)->pluck('name')->first();
    }

    /**
     * @param $id
     * Return first record of merchant where id is $id
     */
    public function find($id)
    {
        return $this->merchants->find($id);
    }

    /**
     * @param $latest_checkin
     * Return name of a merchant from its id history
     */
    public function findMerchantNameByIdHistory($latest_checkin)
    {
        return $this->history->where('history.id', $latest_checkin)
            ->leftJoin('cms_users', 'cms_users.id', 'history.merchants_id')
            ->pluck('cms_users.name')
            ->first();
    }

    /**
     * @param $id
     * @param array $array
     * Update merchant by its id
     */
    public function updateWhereId($id, array $array)
    {
        $this->merchants->where('id', $id)->update($array);
    }
}
