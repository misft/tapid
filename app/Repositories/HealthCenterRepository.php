<?php namespace App\Repositories;

use App\HealthCenter;

class HealthCenterRepository {
    public function get() {
        return HealthCenter::paginate(20);
    }

    public function getWithName($name) {
        return HealthCenter::where('health_center.name', 'like', "%$name%")
            ->leftJoin('district', 'district.code', 'health_center.district_code')
            ->select('health_center.*')
            ->addSelect('district.name as district_name')
            ->groupBy('health_center.id')
            ->paginate(100);
    }
}
