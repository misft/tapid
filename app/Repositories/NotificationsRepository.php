<?php namespace App\Repositories;

use App\Notifications;

class NotificationsRepository
{
    protected $notifications;

    public function __construct(Notifications $notifications)
    {
        $this->notifications = $notifications;
    }

    /**
     * @param $data
     * @return mixed
     * Insert a record
     */
    public function insertGetId($data) {
        return $this->notifications->insertGetId($data);
    }

    /**
     * @param $id
     * Return a record based on the id
     */
    public function find($id)
    {
        return $this->notifications->find($id);
    }
}
