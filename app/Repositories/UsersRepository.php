<?php namespace App\Repositories;

use DB;
use App\Users;
use Illuminate\Support\Facades\Hash;

class UsersRepository {
    protected $users;

    public function __construct(Users $users) {
        $this->users = $users;
    }

    public function find($id) {
        return $this->users->find($id);
    }

    public function findWithEmail($email) {
        return $this->users->where('email', $email)->orderBy('id', 'desc')->first();
    }

    /**
     * @param $phone
     * @return mixed
     * Get the user where phone is $phone.
     * Used for login purpose.
     */
    public function getLoginUser($phone) {
        return $this->users
            ->where('phone', $phone)
            ->orderBy('created_at', 'desc')
            ->first();
    }

    /**
     * @param $phone
     * @param $regid
     * After the user logged in, save regid and update last login
     */
    public function updateAfterLogin($phone, $regid)
    {
        return $this->users
            ->where('phone', $phone)
            ->update([
                'last_login'=>date('Y-m-d H:i:s'),
                'regid'=>$regid
            ]);
    }

    /**
     * @param $id
     * @return mixed
     * Set the user check in flag to 1
     * after user checked in somewhere
     */
    public function updateAfterCheckIn($id) {
        return $this->users->where('id', $id)->update([
            'is_checkin'=>1
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * Set the user check in flag to 0
     * after user checked out
     */
    public function updateAfterCheckOut($id) {
        return $this->users->where('id', $id)->update([
            'is_checkin'=>0
        ]);
    }

    /**
     * @param $id
     * @return mixed
     * Return regid of a user where id is $id
     */
    public function getRegidById($id) {
        return $this->users->where('id', $id)->pluck('regid');
    }

    public function insertGetId($data) {
        return $this->users->insertGetId($data);
    }

    /**
     * @param $phone
     * @param $nik
     * @param $email
     * @return mixed
     * Check if any duplicate data of phone or nik or email exist.
     */
    public function isAnyDuplicateExist($phone, $nik, $email)
    {
        return $this->users
            ->where('phone', 'like', $phone)
            ->orWhere('nik', 'like', $nik)
            ->orWhere('email', 'like', $email)
            ->exists();
    }

    /**
     * @param $nik
     * Return regid where nik is $nik
     */
    public function getRegidByNIK($nik)
    {
        return $this->users->where('nik', $nik)->pluck('regid')->first();
    }

    /**
     * @param $list_user
     * Set every users in notification to active OTG
     */
    public function setOtg($list_user, $timestamp, $id)
    {
        $list = json_decode($list_user);
        foreach($list as $i) {
            $user = $this->users->where('nik', $i->no_identitas)->first();
            $this->users->where('nik', $i->no_identitas)->update([
                'is_otg'=>1
            ]);
            DB::table('otg')->insert([
                'users_id'=>$user->id,
                'start_at'=>$timestamp,
                'notifications_id'=>$id
            ]);
        }
    }

    /**
     * @param $id_user
     * Return regid of a user
     */
    public function getRegidOf($id_user)
    {
        return $this->users->where('id', $id_user)->first()->regid;
    }

    /**
     * @param $email
     * Update password of defined user by email
     */
    public function updatePasswordWithEmail($email, $password)
    {
        $this->users->where('email', $email)->update([
            'password'=>Hash::make($password)
        ]);
    }
}
