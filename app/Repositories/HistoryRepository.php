<?php namespace App\Repositories;

use DB;
use App\History;

class HistoryRepository {
    protected $history;

    public function __construct(History $history) {
        $this->history = $history;
    }

    /**
     * @param $id
     * @return mixed
     * Find history where id is
     */
    public function find($id) {
        return $this->history->find($id);
    }

    /**
     * @param $id_user
     * @return mixed
     * Return id where user last checked in
     */
    public function userLastCheckIn($id_user) {
        return $this->history->where('users_id', $id_user)->whereNull('check_out')->pluck('id')->first();
    }

    /**
     * @param $id_user
     * @return mixed
     * Check if user is currently / lastly checked in
     */
    public function isUserCurrentlyCheckIn($id_user)
    {
        return $this->history->where('users_id', $id_user)->whereNull('check_out')->exists();
    }


    /**
     * @param $id_user
     * @param $id
     * @return mixed
     * Insert record to history when user checked in
     */
    public function insertAfterCheckIn($id_user, $id)
    {
        return $this->history->insertGetId([
            'users_id'=>$id_user,
            'merchants_id'=>$id,
            'check_in'=>date('Y-m-d H:i:s')
        ]);
    }


    /**
     * @param $id_user
     * @return mixed
     * Return id of the last user's check in
     */
    public function getLastIdCheckInUser($id_user)
    {
        return $this->history->where('id_user', $id_user)->whereNull('check_out')->orderBy('check_in', 'desc')->pluck('id')->first();
    }

    public function updateAfterCheckout($id, $check_in)
    {
        $this->history->where('id', $id)->update([
            'check_in'=>$check_in,
            'check_out'=>date("Y-m-d H:i:s")
        ]);
    }

    /**
     * @param $date_start
     * @param $date_end
     * Return clustered history for handling request data from c-trace
     */
    public function getClusteredHistory($date_start, $date_end)
    {
        return $this->history
            ->leftJoin('users', 'users.id', 'history.users_id')
            ->leftJoin('cms_users', 'cms_users.id', 'history.merchants_id')
            ->whereBetween('history.check_in', [$date_start, $date_end])
            ->select(
                'users.nik as no_identitas',
                'users.name as nama',
                'cms_users.jsc_code as code_tenant_jsc',
                'cms_users.name as tenant_name',
                'history.check_in as date_insert'
            )
            ->get();
    }

    /**
     * @param $date_start
     * Return contacted tracing history for handling request data from c-trace
     */
    public function getContactedTracingHistory($date_start)
    {
        return $this->history
            ->leftJoin('users', 'users.id', 'history.users_id')
            ->leftJoin('cms_users', 'cms_users.id', 'history.merchants_id')
            ->where('history.check_in', '>=', $date_start)
            ->select(
                'users.nik as no_identitas',
                'users.name as nama',
                'cms_users.merchant_type as code_tenant_jsc',
                'cms_users.name as tenant_name',
                'history.check_in as date_insert'
            )
            ->get();
    }

    /**
     * @param $id
     * @param $nik
     * @return
     * Return every possible records of history of a user based on its id
     */
    public function getAllHistories($id, $nik)
    {
        $subquery_history_in = DB::table('history')
            ->select(
                'users_id',
                'merchants_id',
                'check_in as time',
                DB::raw("CONVERT(null, char) as message"),
                DB::raw("CONVERT($nik, char) as list_user"),
                DB::raw("CONVERT('check_in', char) as type")
            );
        $subquery_history_out = DB::table('history')
            ->select(
                'users_id',
                'merchants_id',
                'check_out as time',
                DB::raw("CONVERT(null, char) as message"),
                DB::raw("CONVERT($nik, char) as list_user"),
                DB::raw("CONVERT('check_out', char) as type")
            )->whereNotNull('history.check_out');
        $subquery_notifications = DB::table('notifications')
            ->select(
                DB::raw("$id as users_id"),
                DB::raw("0 as merchants_id"),
                'created_at as time',
                DB::raw("CONVERT(message, char) as message"),
                DB::raw("CONVERT(list_user, char) as list_user"),
                DB::raw("CONVERT('third_party', char) as type")
            );

        $union = $subquery_notifications->union($subquery_history_in)->union($subquery_history_out);

        $result = DB::query()->fromSub($union->toSql(), 'histories')
            ->leftJoin('cms_users', 'cms_users.id', 'histories.merchants_id')
            ->leftJoin('users', 'users.id', 'histories.users_id')
            ->orderBy('histories.time', 'desc')
            ->select('histories.*', 'cms_users.name as merchant_name', 'users.name as user_name')
            ->where('histories.list_user', 'like', "%$nik%")
            ->where('histories.users_id', $id)
            ->paginate(20);
        foreach ($result->items() as $i) {
            unset($i->list_user);
        }
        return $result;
    }

    /**
     * @param $id
     * Count currently checked in merchant by merchant id
     */
    public function countCheckInMerchantById($id)
    {
        return $this->history->where('merchants_id', $id)->whereNull('check_out')->count();
    }
}
