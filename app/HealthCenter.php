<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HealthCenter extends Model
{
    protected $table = 'health_center';
}
