<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MerchantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cms_users')->insert([
            'name'=>'Indra Mall',
            'email'=>'indrawandwiprasetyo@gmail.com',
            'password'=>Hash::make('12345678'),
            'id_cms_privileges'=>2,
            'created_at'=>date('Y-m-d H:i:s'),
            'status'=>'Active',
            'address'=>str_random(10),
            'latitude'=>-6.249523,
            'longitude'=>106.795517,
            'capacity'=>9000,
            'qrcode'=>'uploads/1/2020-09/d428396d93f06905e7bcc46a2c47d231.png'
        ]);
    }
}
