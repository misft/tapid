<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nik'=>'3374022911990001',
            'name'=>'Indrawan Dwi',
            'email'=>'indrawandwiprasetyo@gmail.com',
            'password'=>Hash::make('indra123'),
            'address'=>'Jl. Erowati',
            'phone'=>'089668058996',
            'selfie'=>'dsadsa',
            'birth'=>'Semarang, 29 November 1999',
            'gender'=>'L',
            'religion'=>'Islam',
            'nationality'=>'Indonesia',
            'marital_status'=>'Belum Kawin',
            'last_login'=>date('Y-m-d'),
            'verified'=>0,
            'verified_at'=>date('Y-m-d H:i:s'),
            'regid'=>str_random(10),
            'secret'=>str_random(12),
            'qrcode'=>'dummy',
            'work'=>'Programmer'
        ]);
    }
}
