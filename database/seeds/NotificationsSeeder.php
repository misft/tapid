<?php

use Illuminate\Database\Seeder;
use App\Notifications;

class NotificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $object = json_encode([
            str_random(16),
            str_random(16),
            str_random(16)
        ]);
        Notifications::insert([
            'signature'=>str_random(15),
            'message'=>str_random(50),
            'list_user'=>json_encode($object)
        ]);
    }
}
