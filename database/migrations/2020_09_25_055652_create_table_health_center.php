<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHealthCenter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("health_center", function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("city_code");
            $table->string("name");
            $table->string("address");
            $table->double("latitude")->nullable();
            $table->double("longitude")->nullable();
            $table->string("phone");
            $table->timestamp("created_at")->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
