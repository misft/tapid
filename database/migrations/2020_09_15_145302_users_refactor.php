<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersRefactor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("users", function(Blueprint $table) {
            $table->string("ktp");
            $table->dropColumn("birth");
            $table->dropColumn("gender");
            $table->dropColumn("religion");
            $table->dropColumn("nationality");
            $table->dropColumn("marital_status");
            $table->dropColumn("work");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
