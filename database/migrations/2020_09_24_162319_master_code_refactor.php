<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterCodeRefactor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('province');
        Schema::dropIfExists('city');
        Schema::dropIfExists('district');
        Schema::dropIfExists('subdistrict');
        Schema::create('province', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('name', 30);
            $table->timestamp('created_at')->useCurrent();
        });
        Schema::create('city', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('province_code')->nullable();
            $table->string('name', 30);
            $table->timestamp('created_at')->useCurrent();
        });
        Schema::create('district', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('city_code')->nullable();
            $table->string('name', 30);
            $table->timestamp('created_at')->useCurrent();
        });
        Schema::create('subdistrict', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('district_code')->nullable();
            $table->string('name', 30);
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
