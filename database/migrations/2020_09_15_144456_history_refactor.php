<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryRefactor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop("history");
        Schema::create("history", function(Blueprint $table) {
           $table->bigIncrements("id");
           $table->integer("users_id");
           $table->integer("merchants_id");
           $table->timestamp("check_in");
           $table->timestamp("check_out");
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
