<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("nik");
            $table->string("name");
            $table->string("email");
            $table->string("password");
            $table->string("address");
            $table->string("phone");
            $table->string("selfie");
            $table->string("birth");
            $table->char("gender", 2);
            $table->string("religion");
            $table->string("nationality")->nullable();
            $table->string("marital_status")->nullable();
            $table->datetime("last_login")->nullable();
            $table->boolean("verified");
            $table->datetime("verified_at")->nullable();
            $table->string("regid")->nullable();
            $table->string("secret", 20);
            $table->string("qrcode");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
