<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClusterOtg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("cluster_otg", function(Blueprint $table) {
            $table->bigIncrements("id");
            $table->integer("users_nik");
            $table->integer("merchants_id");
            $table->bigInteger("history_id");
            $table->bigInteger("notifications_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("cluster_otg");
    }
}
