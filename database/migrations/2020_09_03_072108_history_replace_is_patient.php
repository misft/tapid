<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryReplaceIsPatient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history', function(Blueprint $table) {
            $table->dropColumn('is_patient');
            $table->dropColumn('patient_day_at');
            $table->boolean('is_pdp');
            $table->boolean('is_odp');
            $table->integer('pdp_day_at');
            $table->integer('odp_day_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
