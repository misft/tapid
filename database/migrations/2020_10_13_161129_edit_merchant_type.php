<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditMerchantType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists("merchant_type");
        Schema::table('cms_users', function(Blueprint $table) {
            $table->renameColumn("from_type_of", "merchant_type_of");
            $table->renameColumn("merchant_type_code", "merchant_type");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
