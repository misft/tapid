<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMasterCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('province', function(Blueprint $table) {
           $table->bigIncrements('id');
           $table->string('name', 30);
           $table->timestamp('created_at')->useCurrent();
        });
        Schema::create('city', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30);
            $table->timestamp('created_at')->useCurrent();
        });
        Schema::create('district', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30);
            $table->timestamp('created_at')->useCurrent();
        });
        Schema::create('subdistrict', function(Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 30);
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
