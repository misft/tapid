<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MerchantsAddColumnsCtrace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_users', function (Blueprint $table) {
            $table->integer("created_by")->nullable();
            $table->integer("from_type_of")->nullable();
            $table->integer("jsc_code")->nullable();
            $table->string("prov_code")->nullable();
            $table->string("prov_name")->nullable();
            $table->string("city_code")->nullable();
            $table->string("city_name")->nullable();
            $table->string("district_code")->nullable();
            $table->string("district_name")->nullable();
            $table->string("subdistrict_code")->nullable();
            $table->string("subdistrict_name")->nullable();
            $table->integer("floor_count")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
