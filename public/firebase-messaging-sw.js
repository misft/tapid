importScripts('https://www.gstatic.com/firebasejs/7.18.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.18.0/firebase-messaging.js');
const firebaseConfig = {
    apiKey: "AIzaSyAag-JGRX__4feGTRdWkekdCYe65JqGO0Q",
    authDomain: "neonik-33c0d.firebaseapp.com",
    databaseURL: "https://neonik-33c0d.firebaseio.com",
    projectId: "neonik-33c0d",
    storageBucket: "neonik-33c0d.appspot.com",
    messagingSenderId: "966676773187",
    appId: "1:966676773187:web:da1279b5ad4f91c0d057f6",
    measurementId: "G-9R715L8PPM"
};
firebase.initializeApp(firebaseConfig);

let messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
        body: payload.notification.title,
        content: payload.notification.body,
        icon: '/images/ic_ios_launcher-min.png'
    };
    const notification = Notification(notificationTitle, notificationOptions);
    notification.onclick = function () {
        window.open("http://tapid.co.id/admin");
    }
    return notification;
});
