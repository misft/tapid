function getCurrentLink() {
    if (window.location.hostname == "localhost") {
        return "http://localhost:8000/"
    } else if (window.location.hostname == "neptune.crocodic.net") {
        return "http://neptune.crocodic.net/tapid/public/"
    }
    return "http://tapid.co.id/"
}

var citySelect = $('select[name=city_code]').select2({
    ajax: {
        url: '/api/list_city',
        data: function (params) {
            return {
                name: params.term
            }
        },
        dataType: 'json',
        processResults: function (res) {
            return {
                results: $.map(res.data, function (item) {
                    return {
                        text: item.name,
                        id: item.code
                    }
                })
            }
        }
    }
});

var districtSelect = $('select[name=district_code]').select2();

citySelect.on("change", function (e) {
    $.ajax({
        url: '/api/list_district',
        data: {
            code: this.value
        }
    }).then(function (res) {
        // create the option and append to Select2
        var options = []
        districtSelect.val(null).trigger('change')
        res.data.forEach(e => {
            options.push(new Option(e.name, e.code, true, true))
        })
        districtSelect.empty()
        districtSelect.val(null).trigger('change')
        districtSelect.append(options).trigger('change');
        options = [];
    });
});
