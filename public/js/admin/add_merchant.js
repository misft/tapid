function getCurrentLink() {
    if(window.location.hostname == "localhost" || "tapid.co.id") {
        return "/"
    }
    else {
        return "/tapid/public"
    }
}

let merchantTypeOfSelect = $('select[name="merchant_type_of"]').select2({disabled: true})
let merchantTypeSelect = $('select[name="merchant_type"]').select2();
merchantTypeSelect.on("change", function (e) {
    if (e.target.value != 5) {
        merchantTypeOfSelect.prop("disabled", true)
    } else {
        merchantTypeOfSelect.prop("dis  abled", false)
    }
});
var map;
var marker;

document.getElementById("map-search").addEventListener("keydown", function (e) {
    if (e.keyIdentifier == 'U+000A' || e.keyIdentifier == 'Enter' || e.keyCode == 13) {
        if (e.target.nodeName == 'INPUT' && e.target.type == 'text') {
            e.preventDefault();
            return false;
        }
    }
})

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: {lat: -6.229728, lng: 106.6894302},
        zoom: 11,
        mapTypeId: "roadmap"
    });
    marker = new google.maps.Marker({
        position: {lat: -6.229728, lng: 106.6894302},
        map: map,
        draggable: false,
        title: 'Hello World!'
    });
    var markers = [];

    const input = document.getElementById("map-search");
    const searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(document.getElementById("map-search-container"));
    // Bias the SearchBox results towards current map's viewport.
    map.addListener("bounds_changed", () => {
        searchBox.setBounds(map.getBounds());
    });
    searchBox.addListener("places_changed", () => {
        const places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        markers = [];

        // For each place, get the icon, name and location.
        const bounds = new google.maps.LatLngBounds();

        markers.forEach(marker => {
            marker.setMap(null);
        });

        places.forEach(place => {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }

            const icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(15, 15)
            };

            $('input[name="address"]').val(place.formatted_address)

            // Create a marker for each place.
            marker.setPosition(place.geometry.location);
            $('input[name="latitude"]').val(place.geometry.location.lat());
            $('input[name="longitude"]').val(place.geometry.location.lng());

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
    $('#map-search').on("keyup", function (e) {
        if (e.keyCode == 13) {
            $('input[name=address]').val(e.target.value);
        }
    })
    google.maps.event.addListener(map, 'click', function (event) {
        markers.forEach(marker => {
            marker.setMap(null)
        })
        marker.setPosition(event.latLng);
        $('input[name="latitude"]').val(event.latLng.lat);
        $('input[name="longitude"]').val(event.latLng.lng);
        map.setZoom(13);
    });
}

//Event handler for password
$("input[name='confirmation_password']").keyup(function (e) {
    if (e.target.value == $("input[name='password']").val()) {
        $('#password-success').show();
        $('#password-warning').hide();
    } else {
        $('#password-success').hide();
        $('#password-warning').show();
    }
});

var bussinessTypeSelect = $('select[name=bussiness_type_id]').select();
var provinceSelect = $('select[name=prov_code]').select2()
var citySelect = $('select[name=city_code]').select2()
var districtSelect = $('select[name=district_code]').select2()
var subdistrictSelect = $('select[name=subdistrict_code]').select2()

$.ajax({
    url: getCurrentLink()+'api/list_bussiness_type'
}).then(function (res) {
    // create the option and append to Select2
    var options = []
    res.data.forEach(e => {
        options.push(new Option(e.name, e.id, false, false  ))
    })
    bussinessTypeSelect.val(null).trigger('change')
    bussinessTypeSelect.append(options).trigger('change');
});

$.ajax({
    url: getCurrentLink()+'api/list_province'
}).then(function (res) {
    // create the option and append to Select2
    var options = []
    res.data.forEach(e => {
        options.push(new Option(e.name, e.code, false, false    ))
    })
    provinceSelect.val(null).trigger('change')
    provinceSelect.append(options).trigger('change');
});

provinceSelect.on("change", function (e) {
    $.ajax({
        url: getCurrentLink()+'api/list_city',
        data: {
            code: this.value
        }
    }).then(function (res) {
        // create the option and append to Select2
        var options = []
        res.data.forEach(e => {
            options.push(new Option(e.name, e.code, true, true))
        })
        citySelect.val(null).trigger('change')
        citySelect.append(options).trigger('change');
    });
})

citySelect.on("change", function (e) {
    $.ajax({
        url: getCurrentLink()+'api/list_district',
        data: {
            code: this.value
        }
    }).then(function (res) {
        // create the option and append to Select2
        var options = []
        res.data.forEach(e => {
            options.push(new Option(e.name, e.code, true, true))
        })
        districtSelect.val(null).trigger('change')
        districtSelect.append(options).trigger('change');
    });
})

districtSelect.on("change", function (e) {
    $.ajax({
        url: getCurrentLink()+'api/list_subdistrict',
        data: {
            code: this.value
        }
    }).then(function (res) {
        // create the option and append to Select2
        var options = []
        res.data.forEach(e => {
            options.push(new Option(e.name, e.code, true, true))
        })
        subdistrictSelect.val(null).trigger('change')
        subdistrictSelect.append(options).trigger('change');
    });
})
