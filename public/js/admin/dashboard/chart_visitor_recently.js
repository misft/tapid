var visitorCountRecently = document.getElementById('visitor_count_currently').getContext("2d");

function getDataset() {
    var arr = [
        {
            label: "Visitor",
            backgroundColor: "blue",
            data: [],
        },
        {
            label: "Capacity",
            backgroundColor: "red",
            data: []
        }
    ]
    responseVisitorRecently.forEach(e => {
        arr[0].data.push(e.visitor_count);
        arr[1].data.push(e.capacity);
    });
    return arr;
}

function getLabel() {
    var arr = [];
    responseVisitorRecently.forEach(e => {
        arr.push(e.name);
    });
    return arr;
}

var data = {
    labels: getLabel(),
    datasets: getDataset()
}
var chartVisitorCountRecently = new Chart(visitorCountRecently, {
    type: 'bar',
    data: data,
    options: {
        barValueSpacing: 20,
        scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                }
            }]
        }
    }
})
