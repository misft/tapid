<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function() {
    Route::get('/', 'AdminController@index');
    Route::get('/visitor_check_in', 'MerchantController@visitorCheckIn');
    Route::get('/clusters', 'AdminController@clusters');
    Route::get('/otg', 'AdminController@otg');
    Route::get('/quarantine', 'AdminController@quarantine');
});


Route::get('/email', 'AdminController@testEmail');

Route::get('/', 'LandingController@index');

Route::get('/privacy_policy', 'LandingController@privacyPolicyPage');

//Forget password
Route::get("/reset_password", 'PasswordController@pageResetPassword');

Route::get("/testAPI", "TestController@test");

Route::get("/terms_of_service", 'LandingController@termsOfServicePage');

Route::prefix('admin/download')->group(function () {
    Route::get('visitor_recently', 'DownloaderController@visitorRecently');
    Route::get('users', 'DownloaderController@users');
    Route::get('merchants', 'DownloaderController@merchants');
    Route::get('otg', 'DownloaderController@otg');
    Route::get('clusters', 'DownloaderController@clusters');
    Route::get('quarantine', 'DownloaderController@quarantine');
    Route::get('merchant_condition', 'DownloaderController@merchantCondition');
    Route::get('overload_merchants', 'DownloaderController@overloadMerchants');

    Route::get('overcapacity_history', 'DownloaderController@overcapacityHistory');
});
